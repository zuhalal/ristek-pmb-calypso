/* eslint-disable no-param-reassign */
export const timestampParser = timestamp => {
  if (!timestamp) {
    return "";
  }

  const dayMap = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday"
  ];

  const monthMap = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];

  const date = timestamp.getDate();
  const month = monthMap[timestamp.getMonth()];
  const year = timestamp.getFullYear();
  const day = dayMap[timestamp.getDay()];
  let hour = String(timestamp.getHours());
  let min = String(timestamp.getMinutes());

  if (hour.length < 2) {
    hour = `0${hour}`;
  }

  if (min.length < 2) {
    min = `0${min}`;
  }

  return `${day}, ${date} ${month} ${year} at ${hour}.${min}`;
};

export const remainingTimeParser = seconds => {
  if (seconds === 0) {
    return "0 second";
  }

  if (!seconds) {
    return "";
  }

  if (seconds < 0) {
    // if deadline overdue
    seconds *= -1;
  }

  const day = Math.floor(seconds / (3600 * 24));
  const hour = Math.floor(seconds / 3600);
  const min = Math.floor((seconds % 3600) / 60);
  const sec = Math.floor((seconds % 3600) % 60);

  let dDisplay = "";

  if (day > 0) {
    dDisplay = day > 0 ? day + (day === 1 ? " day " : " days ") : "";
  } else {
    dDisplay = hour > 0 ? hour + (hour === 1 ? " hour " : " hours ") : "";
  }
  const mDisplay = min > 0 ? min + (min === 1 ? " minute " : " minutes ") : "";
  const sDisplay = sec > 0 ? sec + (sec === 1 ? " second " : " seconds") : "";

  return dDisplay + mDisplay + sDisplay;
};
