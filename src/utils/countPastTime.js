export const countPastTime = date => {
  const now = new Date();
  // Calculate difference time in seconds
  let diff = (now - date) / 1000;
  let output = "";

  if (Math.floor(diff) < 60) {
    output = `${Math.floor(diff)}s`;

    return output;
  }

  // Calculate difference time in minutes
  diff /= 60;

  if (Math.floor(diff) < 60) {
    output = `${Math.floor(diff)}m`;

    return output;
  }

  // Calculate difference time in hours
  diff /= 60;

  if (Math.floor(diff) < 24) {
    output = `${Math.floor(diff)}h`;

    return output;
  }

  // Calculate difference time in days
  diff /= 24;

  if (Math.floor(diff) < 7) {
    output = `${Math.floor(diff)}d`;

    return output;
  }

  diff /= 7;
  output = `${Math.floor(diff)}w`;

  return output;
};
