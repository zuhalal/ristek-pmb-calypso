export const getFriendPageReducer = state => state.friendPageReducer.toJS();
export const getFriends = state => getFriendPageReducer(state).friends;
export const getInterests = state => getFriendPageReducer(state).interests;
export const getFriendsWithInterests = state =>
  getFriendPageReducer(state).friendsWithInterests;
export const getIsLoadingFriendsWithInterests = state =>
  getFriendPageReducer(state).fetching;
export const getIsError = state => getFriendPageReducer(state).error;

export const getStatisticKenalan = state =>
  getFriendPageReducer(state).statistic;
export const isLoadingStatistic = state =>
  getFriendPageReducer(state).isLoadingStatistic;
export const isLoadedStatistic = state =>
  getFriendPageReducer(state).isLoadedStatistic;

export const getFindFriends = state => getFriendPageReducer(state).findFriends;
