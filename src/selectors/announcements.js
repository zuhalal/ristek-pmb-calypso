export const getAnnouncementReducer = state => state.announcementReducer.toJS();
export const getAnnouncements = state =>
  getAnnouncementReducer(state).announcements;
export const isAnnouncementsLoading = state =>
  getAnnouncementReducer(state).isLoading;
export const isAnnouncementsLoaded = state =>
  getAnnouncementReducer(state).isLoaded;

export const getAnnouncementDetailReducer = state =>
  state.announcementDetail.toJS();
export const getAnnouncement = state =>
  getAnnouncementDetailReducer(state).announcement;
export const isAnnouncementLoading = state =>
  getAnnouncementDetailReducer(state).isLoading;
export const isAnnouncementLoaded = state =>
  getAnnouncementDetailReducer(state).isLoaded;
export const getAnnouncementError = state =>
  getAnnouncementDetailReducer(state).error;
