export const isLoggedIn = state => state.global.get("loggedIn");
export const getUser = state => state.global.get("user").toJS();
export const isMaba = state => getUser(state).angkatan === "Maba";

export const getTime = state => state.global.get("serverTime");

export const isLoadingServerTime = state =>
  state.global.get("currentlySending");
