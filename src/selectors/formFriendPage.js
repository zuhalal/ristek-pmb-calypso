export const getFormFriendPageReducer = state =>
  state.formFriendPageReducer.toJS();

export const isFriendLoading = state =>
  getFormFriendPageReducer(state).isFriendLoading;

export const isFriendLoaded = state =>
  getFormFriendPageReducer(state).isFriendLoaded;

export const isInterestsLoading = state =>
  getFormFriendPageReducer(state).isInterestsLoading;

export const isInterestsLoaded = state =>
  getFormFriendPageReducer(state).isInterestsLoaded;

export const isEditPosting = state =>
  getFormFriendPageReducer(state).isEditPosting;

export const isEditSuccess = state =>
  getFormFriendPageReducer(state).isEditSuccess;

export const friend = state => getFormFriendPageReducer(state).friend;
export const interests = state => getFormFriendPageReducer(state).interests;
export const patchedFriend = state =>
  getFormFriendPageReducer(state).patchedFriend;
export const savedFriendsCount = state =>
  getFormFriendPageReducer(state).savedFriendsCount;
export const isLoadedSavedFriendsCount = state =>
  getFormFriendPageReducer(state).isLoadedSavedFriendsCount;
