import React, { Component } from "react";

import { AboutPageContainer } from "./style";
import WhiteCard from "components/WhiteCard";
import Staff from "components/StaffPhoto";
import logo_pmb from "assets/PMB_logo.png";
import ketua_pelaksana from "assets/Ketua_pelaksana.png";
import wakil_ketua_pelaksana from "assets/Wakil_ketua_pelaksana.png";
import sekretaris1 from "assets/Sekretaris1.png";
import sekretaris2 from "assets/Sekretaris2.png";
import bendahara1 from "assets/Bendahara1.png";
import bendahara2 from "assets/Bendahara2.png";
import pj_acara from "assets/PJ_acara.png";
import pj_akademis from "assets/PJ_akademis.png";
import pj_komdis from "assets/PJ_komdis.png";
import pj_hpk from "assets/PJ_hpk.png";
import pj_mentor from "assets/PJ_mentor.png";
import pj_dokum from "assets/PJ_dokum.png";
import pj_medis from "assets/PJ_medis.png";
import pj_danus from "assets/PJ_danus.png";
import pj_penunjang from "assets/PJ_penunjang.png";

class AboutPage extends Component {
  render() {
    return (
      <AboutPageContainer>
        <WhiteCard>
          <div className="flex-row align-center justify-center">
            <img className="logo-pmb" src={logo_pmb} alt="Logo PMB" />
            <div className="content-filosofi flex-column">
              <h1>Filosofi Logo</h1>
              <p>
                PMB 2019 merupakan kesempatan menjelajah dunia baru yang akan
                dimasuki oleh mahasiswa baru. Adapun logo PMB Fasilkom UI 2019
                melambangkan pintu terbuka menuju dunia baru berwujud kota
                metropolis yang berarti kesempatan kepada mahasiswa baru untuk
                mengeksplorasi kehidupan di Fasilkom UI. Warna biru merah pada
                logo melambangkan makara Fasilkom UI dan warna kuning
                melambangkan makara Universitas Indonesia.
              </p>
            </div>
          </div>
        </WhiteCard>

        <WhiteCard>
          <div className="content-visi-misi">
            <h1>Visi PMB 2019</h1>
            <p>
              Menjadikan PMB 2019 sebagai sarana membentuk mahasiswa baru
              Fasilkom UI yang adaptif dan memiliki rasa kekeluargaan terhadap
              Fasilkom UI.
            </p>
          </div>
          <div className="content-visi-misi">
            <h1>Misi PMB 2019</h1>
            <p>
              1. Memperkenalkan mahasiswa baru ke lingkungan akademis, sosial,
              dan budaya baik Fasilkom UI.
              <br />
              2. Menjadi fasilitator antara mahasiswa baru dengan elemen
              Fasilkom UI.
              <br />
              3. Menyelenggarakan rangkaian acara untuk mempererat hubungan
              antara mahasiswa baru dan elemen Fasilkom UI.
              <br />
              4. Menanamkan nilai-nilai PMB 2019 mahasiswa baru dalam segala
              aspek kegiatan PMB 2019
            </p>
          </div>
        </WhiteCard>

        <WhiteCard>
          <div className="content-nilai">
            <h1>Nilai-Nilai PMB 2019</h1>
            <div className="flex-row nilai-box">
              <p className="nilai-pmb">Integritas</p>
              <p className="nilai-pmb">Tanggung Jawab</p>
              <p className="nilai-pmb">Inisiatif</p>
              <p className="nilai-pmb">Kekeluargaan</p>
              <p className="nilai-pmb">Religius</p>
            </div>
          </div>
        </WhiteCard>

        <div className="flex-column justify-center align-center">
          <div className="flex-row justify-center align-center">
            <Staff
              label="Ketua Pelaksana"
              urlPhoto={ketua_pelaksana}
              jabatan="Ketua Pelaksana"
            />
            <Staff
              label="Wakil Ketua Pelaksana"
              urlPhoto={wakil_ketua_pelaksana}
              jabatan="Wakil Ketua Pelaksana"
            />
          </div>
          <div className="flex-row wrap justify-center align-center">
            <Staff
              label="Sekretaris 1"
              urlPhoto={sekretaris1}
              jabatan="Sekretaris 1"
            />
            <Staff
              label="Sekretaris 2"
              urlPhoto={sekretaris2}
              jabatan="Sekretaris 2"
            />
            <Staff
              label="Bendahara 1"
              urlPhoto={bendahara1}
              jabatan="Bendahara 1"
            />
            <Staff
              label="Bendahara 2"
              urlPhoto={bendahara2}
              jabatan="Bendahara 2"
            />
          </div>
          <div className="flex-row justify-center align-center">
            <Staff label="PJ Acara" urlPhoto={pj_acara} jabatan="PJ Acara" />
            <Staff
              label="PJ Akademis"
              urlPhoto={pj_akademis}
              jabatan="PJ Akademis"
            />
            <Staff label="PJ Komdis" urlPhoto={pj_komdis} jabatan="PJ Komdis" />
            <Staff label="PJ HPK" urlPhoto={pj_hpk} jabatan="PJ HPK" />
          </div>
          <div className="flex-row justify-center align-center">
            <Staff label="PJ Mentor" urlPhoto={pj_mentor} jabatan="PJ Mentor" />
            <Staff
              label="PJ Dokumentasi"
              urlPhoto={pj_dokum}
              jabatan="PJ Dokumentasi"
            />
            <Staff label="PJ Medis" urlPhoto={pj_medis} jabatan="PJ Medis" />
            <Staff label="PJ Danus" urlPhoto={pj_danus} jabatan="PJ Danus" />
          </div>
          <div className="flex-row justify-center align-center">
            <Staff
              label="PJ Penunjang"
              urlPhoto={pj_penunjang}
              jabatan="PJ Penunjang"
            />
          </div>
        </div>
      </AboutPageContainer>
    );
  }
}

export default AboutPage;
