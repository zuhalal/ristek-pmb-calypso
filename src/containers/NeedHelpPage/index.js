import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Iframe from "react-iframe";
import { FeedbackPageStyle } from "./style";

export class FeedbackPage extends React.Component {
  googleFormResponsive = width => {
    if (width.matches) {
      return (
        <Iframe
          url="https://docs.google.com/forms/d/e/1FAIpQLSerYNuNsI-L88IEJ9BVRBnkYUCwl9yXaXc05knaxZ-xeuBeLg/viewform"
          width="700em"
          height="700em"
          className="googleForm"
        />
      );
    }

    return (
      <Iframe
        url="https://docs.google.com/forms/d/e/1FAIpQLSerYNuNsI-L88IEJ9BVRBnkYUCwl9yXaXc05knaxZ-xeuBeLg/viewform"
        width="100%"
        height="700em"
        className="googleForm"
      />
    );
  };

  render() {
    const width = window.matchMedia("(min-width: 64em)");

    return (
      <FeedbackPageStyle>
        <div className="googleFormContainer">
          {!this.props.isMaba ? (
            <h1>Sorry, this page is only accessible for new students</h1>
          ) : (
            <>
              <p>Tell us your problem. We will help you!</p>
              {this.googleFormResponsive(width)}
            </>
          )}
        </div>
      </FeedbackPageStyle>
    );
  }
}

FeedbackPage.propTypes = {
  isMaba: PropTypes.bool.isRequired
};

function mapStateToProps(state) {
  return {
    isMaba: state.profileReducer.get("isUserMaba")
  };
}

export default connect(
  mapStateToProps,
  null
)(FeedbackPage);
