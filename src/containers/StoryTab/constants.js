export const FETCH_STORIES = "src/StoryTab/FETCH_STORIES";

export const FETCH_STORIES_SUCCESS = "src/StoryTab/FETCH_STORIES_SUCCESS";

export const FETCH_STORIES_FAILED = "src/StoryTab/FETCH_RECENT_FAILED";

export const DELETE_STORY = "src/StoryTab/DELETE_STORY";

export const DELETE_STORY_SUCCESS = "src/StoryTab/DELETE_STORY_SUCCESS";

export const DELETE_STORY_FAILED = "src/StoryTab/DELETE_STORY_FAILED";

export const RESET_STATE = "src/StoryTab/RESET_STATE";
