import axios from "axios";
import swal from "sweetalert";
import {
  FETCH_ANNOUNCEMENT_DETAIL,
  FETCH_ANNOUNCEMENT_DETAIL_SUCCESS,
  FETCH_ANNOUNCEMENT_DETAIL_FAILED,
  POST_QNA_COMMENT,
  POST_QNA_COMMENT_SUCCESS,
  POST_QNA_COMMENT_FAILED,
  CLEAR_ANNOUNCEMENT_DETAIL
} from "./constants";

import { fetchAnnouncementDetailApi, postCommentApi } from "api";

export function fetchAnnouncementDetail(id) {
  return dispatch => {
    dispatch({ type: FETCH_ANNOUNCEMENT_DETAIL });
    axios
      .get(fetchAnnouncementDetailApi(id))
      .then(response => {
        dispatch({
          payload: response.data,
          type: FETCH_ANNOUNCEMENT_DETAIL_SUCCESS
        });
      })
      .catch(error => {
        dispatch({
          payload: error,
          type: FETCH_ANNOUNCEMENT_DETAIL_FAILED
        });
      });
  };
}

export function postAnnouncementComment(comment) {
  return dispatch => {
    dispatch({ type: POST_QNA_COMMENT });
    axios
      .post(postCommentApi, comment)
      .then(() => {
        dispatch({ type: POST_QNA_COMMENT_SUCCESS });
        dispatch(fetchAnnouncementDetail(comment.post_id));
        swal("Comment has been added", "", "success");
      })
      .catch(error => {
        dispatch({
          payload: error,
          type: POST_QNA_COMMENT_FAILED
        });
        swal(
          "Error",
          "Failed to add a comment. Please try again later",
          "error"
        );
      });
  };
}

export function clearAnnouncement() {
  return dispatch => {
    dispatch({ type: CLEAR_ANNOUNCEMENT_DETAIL });
  };
}
