/* eslint-disable no-case-declarations */
/* eslint-disable array-callback-return */
import { fromJS } from "immutable";
import {
  FETCH_ANNOUNCEMENT_DETAIL,
  FETCH_ANNOUNCEMENT_DETAIL_SUCCESS,
  FETCH_ANNOUNCEMENT_DETAIL_FAILED,
  CLEAR_ANNOUNCEMENT_DETAIL
} from "./constants";

const initialState = fromJS({
  announcement: {},
  error: null,
  isLoaded: false,
  isLoading: false
});

function announcementDetailPageReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_ANNOUNCEMENT_DETAIL:
      return state.set("isLoading", true);
    case FETCH_ANNOUNCEMENT_DETAIL_SUCCESS:
      return state
        .set("announcement", action.payload)
        .set("isLoading", false)
        .set("isLoaded", true);
    case FETCH_ANNOUNCEMENT_DETAIL_FAILED:
      return state
        .set("error", action.payload)
        .set("isLoading", false)
        .set("isLoaded", false);
    case CLEAR_ANNOUNCEMENT_DETAIL:
      return state.set("error", null).set("isLoaded", false);
    default:
      return state;
  }
}

export default announcementDetailPageReducer;
