import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { push } from "connected-react-router";

import DetailCard from "components/DetailCard";
import CommentSection from "components/CommentSection";
import Spinner from "components/Loading";
import NotFoundPage from "components/NotFoundPage";

import {
  fetchAnnouncementDetail,
  postAnnouncementComment,
  clearAnnouncement
} from "./actions";
import {
  getAnnouncement,
  isAnnouncementLoaded,
  isAnnouncementLoading,
  getAnnouncementError
} from "selectors/announcements";

class AnnouncementDetailPage extends Component {
  componentDidMount() {
    const param = this.getParam();
    this.props.fetchAnnouncementDetail(param);
  }

  componentWillUnmount() {
    this.props.clearAnnouncement();
  }

  getParam = () => {
    const params = this.props.location.pathname.split("/")[2];

    return params;
  };

  renderQnA = () => {
    const { announcement } = this.props;
    if (announcement) {
      return (
        <CommentSection
          comments={announcement.qnas}
          onSubmitComment={this.addQnAComment}
        />
      );
    }

    return null;
  };

  addQnAComment = comment => {
    const requestBodyComment = {
      post_type: "announcement",
      post_id: this.getParam(),
      comment
    };

    this.props.postAnnouncementComment(requestBodyComment);
  };

  render() {
    const {
      announcement,
      isLoadedAnnouncement,
      announcementError
    } = this.props;

    return (
      <>
        {isLoadedAnnouncement ? (
          [
            <DetailCard
              key={announcement.title}
              type="announcement"
              title={announcement.title}
              date={announcement.date}
              description={announcement.description}
              attachmentLink={announcement.attachment_link}
              onClick={() => this.props.push("/announcements")}
              isLoading={this.props.isLoadingAnnouncement}
            />,
            this.renderQnA()
          ]
        ) : announcementError &&
          announcementError.response &&
          announcementError.response.data &&
          announcementError.response.data.detail === "Not found." ? (
          <NotFoundPage />
        ) : (
          <Spinner />
        )}
      </>
    );
  }
}

AnnouncementDetailPage.propTypes = {
  announcement: PropTypes.shape().isRequired,
  isLoadedAnnouncement: PropTypes.bool.isRequired,
  isLoadingAnnouncement: PropTypes.bool.isRequired,
  fetchAnnouncementDetail: PropTypes.func.isRequired,
  location: PropTypes.shape().isRequired,
  push: PropTypes.func.isRequired,
  postAnnouncementComment: PropTypes.func.isRequired,
  announcementError: PropTypes.object,
  clearAnnouncement: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  announcement: getAnnouncement(state),
  isLoadedAnnouncement: isAnnouncementLoaded(state),
  isLoadingAnnouncement: isAnnouncementLoading(state),
  announcementError: getAnnouncementError(state)
});

const mapDispatchToProps = dispatch => ({
  fetchAnnouncementDetail: id => dispatch(fetchAnnouncementDetail(id)),
  push: url => dispatch(push(url)),
  postAnnouncementComment: comment =>
    dispatch(postAnnouncementComment(comment)),
  clearAnnouncement: () => dispatch(clearAnnouncement())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AnnouncementDetailPage);
