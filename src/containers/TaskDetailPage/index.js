/* eslint-disable array-callback-return */
/* eslint-disable no-shadow */
import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { push } from "connected-react-router";
import moment from "moment-timezone";

import { TaskDetailPageContainer } from "./style";
import { remainingTimeParser, timestampParser } from "utils/timestampParser";
import getLocalTime from "utils/localTime";

import Spinner from "components/Loading";
import DetailCard from "../../components/DetailCard";
import Section from "../../components/Section";
import Files from "react-files";
import Button from "../../components/Button";
import Card from "components/Card";
import NotFoundPage from "components/NotFoundPage";
import swal from "sweetalert";

import PDFIcon from "assets/icons/pdf.png";
import CommentSection from "components/CommentSection";

import { isEmpty } from "lodash";

import {
  isTaskLoading,
  isTaskLoaded,
  task,
  error,
  isSubmissionLoaded,
  isSubmissionLoading,
  submissions,
  submissionResponse
} from "selectors/taskDetailPage";
import { getUser } from "selectors/user";

import {
  fetchTask,
  fetchTaskSubmissions,
  postTaskSubmissions,
  postTaskComment,
  clearTask
} from "./actions";

class TaskDetailPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      submittedFiles: [],
      timeRemaining: 0,
      task: this.props.task,
      isTaskLoading: this.props.isTaskLoading,
      submissions: this.props.submissions,
      isSubmissionLoading: this.props.isSubmissionLoading,
      date: getLocalTime(),
      newUploadFile: false
    };

    this.handleSubmissionChange = this.handleSubmissionChange.bind(this);
    this.handleSubmissionError = this.handleSubmissionError.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.filterUserSubmission = this.filterUserSubmission.bind(this);
  }

  componentDidMount() {
    this.timer = setInterval(() => this.tick(), 1000);
    const id = this.props.match.params.id;
    this.props.fetchTask(id);
    this.props.fetchTaskSubmissions(id);
  }

  componentDidUpdate(prevProps) {
    if (prevProps !== this.props) {
      if (prevProps.task !== this.props.task) {
        this.setState({ task: this.props.task });
      }
      if (prevProps.isTaskLoading !== this.props.isTaskLoading) {
        this.setState({ isTaskLoading: this.props.isTaskLoading });
      }
      if (prevProps.submissions !== this.props.submissions) {
        if (isEmpty(this.props.submissions)) {
          // handle async state making submittedFiles persist from previous state
          this.setState({ submittedFiles: [] });
        }
        this.setState({ submissions: this.props.submissions });
      }
      if (prevProps.isSubmissionLoading !== this.props.isSubmissionLoading) {
        this.setState({ isSubmissionLoading: this.props.isSubmissionLoading });
      }
    }
  }

  componentWillUnmount() {
    clearInterval(this.timer);
    this.props.clearTask();
  }

  tick() {
    if (this.props.task) {
      const deadline = moment(new Date(this.props.task.end_date));
      const remainingTime = deadline.diff(this.state.date, "seconds");

      this.setState({
        timeRemaining: remainingTime,
        date: getLocalTime()
      });
    }
  }

  handleSubmissionChange(files) {
    // add updated_at property to files
    if (!isEmpty(files)) {
      Reflect.defineProperty(files[0], "updated_at", {
        value: new Date(Date.now())
      });
    }

    this.setState({
      submittedFiles: files,
      newUploadFile: true
    });
  }

  handleSubmissionError(error) {
    swal("File Submit Error", error.message, "error");
  }

  renderSubmittedFilesAsIcons(fileArray) {
    const file = fileArray[0];

    return (
      <div className="file-display-wrap" key={file.id}>
        <div className="icon">
          <img src={PDFIcon} alt="pdf icon" />
        </div>
        <div className="file-name">{file.name}</div>
      </div>
    );
  }

  handleSubmit() {
    const files = this.state.submittedFiles;
    const taskId = this.props.match.params.id;
    const datetime = getLocalTime();
    const submission = {
      file: files[0],
      taskId,
      time: new Date(datetime)
    };

    this.props.postTaskSubmissions(submission);
    this.setState({
      newUploadFile: false
    });
  }

  filterUserSubmission(submission) {
    const fileObj = [];
    submission.map(file => {
      const fileUser = file.user.username;
      if (fileUser === this.props.user.username) {
        const fileLink = file.file_link;
        const substrStartIndex = fileLink.indexOf("/media/") + 7;
        const fileName = fileLink.substring(substrStartIndex, fileLink.length);

        const obj = {
          name: fileName,
          updated_at: file.updated_at
        };
        fileObj.push(obj);
        this.setState({ submittedFiles: fileObj });
      }
    });
  }

  renderQnA = () => {
    const { task } = this.props;
    if (task) {
      return (
        <CommentSection
          comments={task.qnas}
          onSubmitComment={this.addQnAComment}
        />
      );
    }

    return null;
  };

  addQnAComment = comment => {
    const requestBodyComment = {
      post_type: "task",
      post_id: this.props.match.params.id,
      comment
    };

    this.props.postTaskComment(requestBodyComment);
  };

  render() {
    const task = this.state.task;
    const isTaskLoading = this.state.isTaskLoading;
    const megabyte = 1048576;
    const timeRemaining = this.state.timeRemaining;
    const submittedFiles = this.state.submittedFiles;
    const isSubmissionLoading = this.state.isSubmissionLoading;
    const submission = this.state.submissions;
    const { error } = this.props;

    if (submission && submittedFiles.length === 0) {
      this.filterUserSubmission(submission);
    }
    const renderTask = [];
    let isSubmissionNeeded = false;

    if (task) {
      isSubmissionNeeded = task.can_have_submission;

      renderTask.push(
        <DetailCard
          key={`${task.title};${task.start_date}`}
          type="task"
          title={task.title}
          date={new Date(task.end_date)}
          description={task.description}
          attachmentString="task attachment"
          attachmentLink={task.attachment_link}
          onClick={() => this.props.push("/tasks")}
        />
      );
    }

    return (
      <TaskDetailPageContainer>
        {isTaskLoading ? (
          <Spinner />
        ) : error &&
          error.response &&
          error.response.data &&
          error.response.data.detail === "Not found." ? (
          <NotFoundPage />
        ) : (
          [
            renderTask,
            this.props.isMaba && (
              <Section title="Submission">
                <Card className="submission-card">
                  {isSubmissionNeeded ? (
                    <>
                      <div className="dropzone-header">
                        {timeRemaining > 0 ? (
                          <div className="time-remaining">
                            Time Remaining: {remainingTimeParser(timeRemaining)}
                          </div>
                        ) : (
                          <div className="time-remaining">
                            Time Remaining:{" "}
                            <span className="red">
                              Task is overdue by:{" "}
                              {remainingTimeParser(timeRemaining)}
                            </span>
                          </div>
                        )}
                        {submittedFiles.length !== 0 && (
                          <>
                            <div className="file-name">
                              File Submitted:{" "}
                              <span className="green">
                                {submittedFiles[0].name}
                              </span>
                            </div>
                            <div className="time-remaining">
                              File Submitted on :{" "}
                              <span className="green">
                                {timestampParser(
                                  new Date(submittedFiles[0].updated_at)
                                )}
                              </span>
                            </div>
                          </>
                        )}
                        <p style={{ fontSize: "12px" }}>Maximum size: 10MB</p>
                      </div>
                      <Files
                        className="dropzone"
                        multiple={false}
                        clickable={true}
                        maxFileSize={10 * megabyte}
                        accepts={[
                          ".pdf",
                          ".docx",
                          ".doc",
                          ".zip",
                          ".rar",
                          ".tar.gz"
                        ]}
                        onChange={this.handleSubmissionChange}
                        onError={this.handleSubmissionError}
                      >
                        {submittedFiles.length === 0 ? (
                          <p className="dropzone-content">
                            Drag file here or click to upload
                          </p>
                        ) : (
                          <>
                            {this.renderSubmittedFilesAsIcons(submittedFiles)}
                          </>
                        )}
                      </Files>
                      <div className="button-wrap" onClick={this.handleSubmit}>
                        {this.state.newUploadFile && <Button text="Submit" />}
                        {isSubmissionLoading ? <span>submitting...</span> : ""}
                      </div>
                    </>
                  ) : (
                    <div className="dropzone-header">
                      <div className="time-remaining green"> No Submission</div>
                    </div>
                  )}
                </Card>
              </Section>
            ),
            this.renderQnA()
          ]
        )}
      </TaskDetailPageContainer>
    );
  }
}

TaskDetailPage.propTypes = {
  push: PropTypes.func.isRequired,
  postTaskSubmissions: PropTypes.func.isRequired,
  fetchTaskSubmissions: PropTypes.func.isRequired,
  fetchTask: PropTypes.func.isRequired,
  match: PropTypes.shape().isRequired,
  task: PropTypes.shape().isRequired,
  user: PropTypes.shape().isRequired,
  isTaskLoading: PropTypes.bool.isRequired,
  isTaskLoaded: PropTypes.bool.isRequired,
  submissions: PropTypes.array.isRequired,
  isSubmissionLoading: PropTypes.bool.isRequired,
  // serverTime: PropTypes.object.isRequired,
  postTaskComment: PropTypes.func.isRequired,
  isMaba: PropTypes.bool.isRequired,
  error: PropTypes.object,
  clearTask: PropTypes.func.isRequired
};

function mapStateToProps(state) {
  return {
    isTaskLoaded: isTaskLoaded(state),
    isTaskLoading: isTaskLoading(state),
    task: task(state),
    isSubmissionLoaded: isSubmissionLoaded(state),
    isSubmissionLoading: isSubmissionLoading(state),
    submissions: submissions(state),
    error: error(state),
    submissionResponse: submissionResponse(state),
    user: getUser(state),
    // serverTime: state.global.get("serverTime"),
    isMaba: state.profileReducer.get("isUserMaba")
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchTask: id => dispatch(fetchTask(id)),
    fetchTaskSubmissions: id => dispatch(fetchTaskSubmissions(id)),
    postTaskSubmissions: submission =>
      dispatch(postTaskSubmissions(submission)),
    push: url => dispatch(push(url)),
    postTaskComment: comment => dispatch(postTaskComment(comment)),
    clearTask: () => dispatch(clearTask())
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TaskDetailPage);
