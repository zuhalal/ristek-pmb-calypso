import styled from "styled-components";

export const TokenInputContainer = styled.div`
  color: ${props => props.theme.colors.primaryBlack};

  .input-token-field {
    width: 100%;
    border: 1px solid ${props => props.theme.colors.blue};
    background: ${props => props.theme.colors.fieldGray};
    box-sizing: border-box;
    border-radius: 2px;
    margin: 0.5rem 0;
    margin-bottom: 1rem;
    height: 2.5rem;
    font-size: 1.6rem;
    padding: 0.5rem;
  }

  .btn-cont {
    display: flex;
    justify-content: flex-end;
    p {
      font-size: 14px;
    }
  }

  @media (max-width: 880px) {
    .desc-text {
      font-size: 14px;
    }
  }

  @media (max-width: 450px) {
    .input-token-field {
      height: 2.2rem;
      font-size: 1.2rem;
    }
  }

  @media (max-width: 360px) {
    .desc-text {
      font-size: 13px;
    }

    .btn-cont {
    p {
      font-size: 12px;
    }
  }
`;
