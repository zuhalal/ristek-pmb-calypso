import styled from "styled-components";

export const TaskPageContainer = styled.div`
  .task-list-wrap {
    margin: 1rem 0;
  }

  .title {
    font-size: 20px;
  }

  @media (max-width: 600px) {
    .title {
      font-size: 16px;
    }
  }
`;
