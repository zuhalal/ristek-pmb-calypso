import axios from "axios";

import {
  FETCH_GROUP,
  FETCH_GROUP_SUCCESS,
  FETCH_GROUP_FAILED,
  PATCH_EDIT_PROFILE,
  PATCH_EDIT_PROFILE_SUCCESS,
  PATCH_EDIT_PROFILE_FAILED,
  UPLOAD_PROFILE_PICTURE,
  UPLOAD_PROFILE_PICTURE_SUCCESS,
  UPLOAD_PROFILE_PICTURE_FAILED,
  RESET_STATE
} from "./constants";

import { UPDATE_PROFILE } from "../ProfilePage/constants";

import { fetchGroupApi, postEditProfileApi, uploadPictureApi } from "api";

import swal from "sweetalert";

export function fetchGroup() {
  return dispatch => {
    dispatch({
      type: FETCH_GROUP
    });
    axios
      .get(fetchGroupApi)
      .then(response => {
        dispatch({
          payload: response.data,
          type: FETCH_GROUP_SUCCESS
        });
      })
      .catch(error => {
        dispatch({
          payload: error,
          type: FETCH_GROUP_FAILED
        });
      });
  };
}

export function resetState() {
  return dispatch =>
    dispatch({
      type: RESET_STATE
    });
}

export function editProfile(formData, object, shouldUpload) {
  const config = {
    headers: {
      "content-type": "multipart/form-data"
    }
  };

  return dispatch => {
    if (shouldUpload.upload) {
      dispatch({
        type: UPLOAD_PROFILE_PICTURE
      });
      axios
        .post(uploadPictureApi, formData, config)
        .then(responseUpload => {
          dispatch({
            type: PATCH_EDIT_PROFILE
          });
          dispatch({
            type: UPLOAD_PROFILE_PICTURE_SUCCESS
          });
          object.profil.foto = responseUpload.data.file;
          axios
            .patch(postEditProfileApi, object)
            .then(responseEdit => {
              dispatch({
                payload: responseEdit.data,
                type: UPDATE_PROFILE
              });
              dispatch({
                payload: responseEdit.data,
                type: PATCH_EDIT_PROFILE_SUCCESS
              });
              swal("Successful", "Your profile has been updated !", "success");
            })
            .catch(error => {
              dispatch({
                payload: error.message,
                type: PATCH_EDIT_PROFILE_FAILED
              });
              swal(
                "Error",
                `Failed to update your profile ${error.message}`,
                "error"
              );
            });
        })
        .catch(error => {
          dispatch({
            payload: error.message,
            type: UPLOAD_PROFILE_PICTURE_FAILED
          });
          swal(
            "Error",
            `Failed to update your profile ${error.message}`,
            "error"
          );
        });
    } else {
      dispatch({
        type: PATCH_EDIT_PROFILE
      });
      if (shouldUpload.delete) {
        object.profil.foto = null;
      }
      axios
        .patch(postEditProfileApi, object)
        .then(responseEdit => {
          dispatch({
            payload: responseEdit.data,
            type: UPDATE_PROFILE
          });
          dispatch({
            payload: responseEdit.data,
            type: PATCH_EDIT_PROFILE_SUCCESS
          });
          swal("Successful", "Your profile has been updated !", "success");
        })
        .catch(error => {
          dispatch({
            payload: error.message,
            type: PATCH_EDIT_PROFILE_FAILED
          });
          swal(
            "Error",
            `Failed to update your profile ${error.message}`,
            "error"
          );
        });
    }
  };
}
