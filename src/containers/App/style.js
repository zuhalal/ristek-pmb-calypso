import styled from "styled-components";

export const AppContainer = styled.div`
  display: flex;
  flex-direction: column;
  position: relative;
  box-sizing: border-box;
  width: 100%;
  max-width: 100%;
  overflow: hidden;
  min-height: 100vh;
  background: ${props => props.theme.colors.boneWhite};
  z-index: 1;

  h1,
  h2,
  h3,
  b,
  strong {
    font-weight: ${props => props.theme.weight.bold};
  }
`;
