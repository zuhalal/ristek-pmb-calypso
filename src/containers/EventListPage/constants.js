export const FETCH_EVENT_LIST = "src/EventListPage/FETCH_EVENT_LIST";
export const FETCH_EVENT_LIST_SUCCESS =
  "src/EventListPage/FETCH_EVENT_LIST_SUCCESS";
export const FETCH_EVENT_LIST_FAILED =
  "src/EventListPage/FETCH_EVENT_LIST_FAILED";
