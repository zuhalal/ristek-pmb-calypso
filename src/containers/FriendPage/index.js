/* eslint-disable no-confusing-arrow */
import React, { Component } from "react";
import { connect } from "react-redux";
import {
  fetchFriends,
  fetchStatisticKenalan,
  clearStatisticsIsLoaded,
  fetchFindFriend
} from "./actions";
import {
  getFriends,
  getInterests,
  getFriendsWithInterests,
  getIsLoadingFriendsWithInterests,
  getIsError,
  getStatisticKenalan,
  isLoadingStatistic,
  isLoadedStatistic,
  getFindFriends
} from "selectors/friend.js";
import { FriendPageStyle } from "./style";
import PropTypes from "prop-types";
import { isEqual } from "lodash";

import { isEditSuccess, patchedFriend } from "selectors/formFriendPage";
import { editFriend, editApproveKenalan } from "../FormFriendPage/actions";

import Buttons from "../../components/Buttons";
import EmptyCard from "../../components/EmptyCard";
import Section from "../../components/Section";
import SearchBar from "../../components/SearchBar";
import StatisticsCard from "../../components/StatisticsCard";
import Spinner from "components/Loading";
import Dropdown from "../../components/Dropdown";
import Pagination from "../../components/Pagination";
import FriendCard from "../../components/FriendCard";
import FindFriendCard from "../../components/FindFriendCard";

const Tab = {
  MY_FRIENDS: 0,
  FIND_FRIENDS: 1
};

class FriendPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchQuery: "",
      statusSelection: "",
      majorSelection: "",
      classSelection: "",
      currentPage: 1,
      paginatedRecords: [],
      isDesktop: null,
      activeTab: Tab.MY_FRIENDS,
      interestSuggestions: []
    };

    this.predefinedInterests = [
      "Game Development",
      "Competitive Programming",
      "Network Security and Operating System",
      "Data Science",
      "Business Intelligence",
      "Web Development",
      "Backend Development",
      "Frontend Development",
      "Mobile Development",
      "UI/UX",
      "Embedded System",
      "Artificial Intelligence",
      "Project Management",
      "Internet of Things",
      "Business IT",
      "Leadership",
      "Social Act",
      "Travelling",
      "Fashion",
      "Automotive",
      "Debate",
      "Cooking",
      "Gardening",
      "Puzzle",
      "History",
      "Astronomy",
      "Music",
      "Band",
      "Vocal",
      "Dance",
      "Theater",
      "Drawing",
      "Painting",
      "Crafting",
      "Graphic Design",
      "Literature",
      "Writing",
      "Photography",
      "Videography",
      "Football",
      "Basketball",
      "Volleyball",
      "Softball",
      "Cricket",
      "Floorball",
      "Golf",
      "Bowling",
      "Badminton",
      "Tennis",
      "Table Tennis",
      "Martial Art",
      "Pencak Silat",
      "Taekwondo",
      "Boxing",
      "Karate",
      "Javelin",
      "Archery",
      "Darts",
      "Chess",
      "Bridge",
      "Athletic",
      "Gym",
      "Swimming",
      "Running",
      "Cycling",
      "Equestrian",
      "Ice Skating",
      "Hiking",
      "Diving",
      "Surfing",
      "Jogging",
      "Yoga",
      "Rock",
      "Jazz",
      "Classic",
      "Pop",
      "R&B",
      "Hip hop",
      "Electric",
      "Alternative",
      "K-Pop",
      "J-Pop",
      "Indie",
      "Netflix",
      "Anime",
      "Thriller",
      "Action",
      "Science Fiction",
      "Comedy",
      "Romance",
      "Superheroes",
      "Star Wars",
      "Lord of The Rings",
      "Game of Thrones",
      "Harry Potter",
      "DotA 2",
      "Pokemon",
      "Counter Strike",
      "League of Legends",
      "Osu",
      "GTA",
      "Minecraft",
      "Visual Novel",
      "FPS Game",
      "Rhytm Game",
      "PUBG",
      "Mobile Legend",
      "Clash Royale",
      "Clash of Clans",
      "Brawl Stars",
      "Marvel",
      "DC Comics",
      "Novel",
      "Comics"
    ];

    /* information about dropdowns used
      - haveAnyOption: an option with value "", this case used to denote no
        preference for that filter category
      - matcher: function that returns boolean, denote wether record will be
        included in result */
    this.dropdowns = [
      {
        placeholder: "Status",
        stateName: "statusSelection",
        haveAnyOption: true,
        anyOptionPlaceholder: "Any status",
        options: ["Saved", "Pending", "Rejected", "Accepted"],
        getField: record => record.status,
        matcher: (friendStatus, dropdownValue) =>
          friendStatus === dropdownValue.toUpperCase(),
        isShow: () => this.state.activeTab === Tab.MY_FRIENDS
      },
      {
        placeholder: "Class of",
        stateName: "classSelection",
        haveAnyOption: true,
        anyOptionPlaceholder: "any class",
        options: ["2018", "2017", "2016", "2015--"],
        getField: record => {
          if (this.state.activeTab === Tab.MY_FRIENDS) {
            return this.props.isUserMaba
              ? record.user_elemen.profil.angkatan.tahun
              : record.user_maba.profil.angkatan.tahun;
          }

          return record.user_detail.profil.angkatan.tahun;
        },
        matcher: (friendClass, dropdownValue) => {
          if (dropdownValue === "2015--") {
            return parseInt(friendClass, 10) <= 2015;
          }

          return friendClass === dropdownValue;
        },
        isShow: () => this.props.isUserMaba
      }
    ];
    this.dropdownsFindFriends = [
      {
        placeholder: "Class of",
        stateName: "classSelection",
        haveAnyOption: true,
        anyOptionPlaceholder: "any class",
        options: ["2018", "2017", "2016", "2015--"],
        getField: record => record.user_detail.profil.angkatan.tahun,
        matcher: (friendClass, dropdownValue) => {
          if (dropdownValue === "2015--") {
            return parseInt(friendClass, 10) <= 2015;
          }

          return friendClass === dropdownValue;
        },
        isShow: () => this.props.isUserMaba
      }
    ];

    this.filterBySearchQuery = this.filterBySearchQuery.bind(this);
    this.filterByDropdowns = this.filterByDropdowns.bind(this);
    this.resize = this.resize.bind(this);
    this.getHandleChangeSearchbarFunction = this.getHandleChangeSearchbarFunction.bind(
      this
    );
  }

  // enum for fields indexed by search bar
  static fieldType = {
    ARRAY: "array",
    NON_ARRAY: "non_array"
  };

  /*
  each element in array represents a field indexed by search bar
  - type: distinguish wether to index an string/number or an array
  - getField: function to get associated field to index relative to an element in
    outermost array
  - getItem: function used if type is fieldType.ARRAY, to get item indexed
    relative to associated field
  */

  getFieldsIndexedBySearchBar = () => [
    {
      type: FriendPage.fieldType.NON_ARRAY,
      getField: record =>
        this.props.isUserMaba
          ? record.user_elemen.profil.nama_lengkap
          : record.user_maba.profil.nama_lengkap
    },
    {
      type: FriendPage.fieldType.NON_ARRAY,
      getField: record =>
        this.props.isUserMaba
          ? record.user_elemen.title
          : record.user_maba.title
    },
    {
      type: FriendPage.fieldType.ARRAY,
      getField: record =>
        this.props.isUserMaba
          ? record.user_elemen.interests
          : record.user_maba.interests,
      getItem: interest => interest.interest_name
    }
  ];

  static numberOfFriendsPerPage = 5;

  // must be odd number
  static numberOfPageNumbersShown = 5;

  componentDidMount() {
    window.addEventListener("resize", this.resize.bind(this));
    this.props.fetchFriends();
  }

  componentDidUpdate() {
    if (this.props.isUserMaba && !this.props.isLoadedStatistic) {
      this.fetchStatisticsOnce();
    }
  }

  fetchStatisticsOnce = (() => {
    let executed = false;

    return () => {
      if (!executed) {
        executed = true;
        this.props.fetchStatisticKenalan();
      }
    };
  })();

  UNSAFE_componentWillMount() {
    this.resize();
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.resize.bind(this));
    this.props.clearStatisticsIsLoaded();
  }

  shouldComponentUpdate(nextProps, nextState) {
    // prevent infinite render loop by pagination
    if (isEqual(this.state, nextState) && isEqual(this.props, nextProps)) {
      return false;
    }

    return true;
  }

  // passed to dropdown and search bar component to change this component's
  // state selections or search query
  getHandleChangeFunction = associatedState => newValue => {
    if (newValue === this.state[associatedState]) {
      return;
    }
    const newState = { currentPage: 1 };
    newState[associatedState] = newValue;
    this.setState(newState);
  };

  getHandleChangeSearchbarFunction = associatedState => newValue => {
    if (this.state.activeTab === Tab.FIND_FRIENDS) {
      this.setState({
        interestSuggestions: this.predefinedInterests.filter(interest =>
          new RegExp(this.state.searchQuery, "i").test(interest)
        )
      });
    }
    if (newValue === this.state[associatedState]) {
      return;
    }
    const newState = { currentPage: 1 };
    newState[associatedState] = newValue;
    this.setState(newState);
  };

  // called when component rerenders (when dropdown or search bar change value)
  // returns result after filtered by dropdowns and search query
  handleAnyFormChange = () => {
    // add conditional for find friends tab
    if (this.state.activeTab === Tab.MY_FRIENDS) {
      const friends = this.props.friendsWithInterests;

      return this.filterBySearchQuery(
        this.filterByDropdowns(this.filterFriends(friends), this.dropdowns),
        this.getFieldsIndexedBySearchBar()
      );
    }

    return this.filterByDropdowns(
      this.props.findFriends,
      this.dropdownsFindFriends
    );
  };

  // change current page state when page changes
  handlePageChange = newPage => this.setState({ currentPage: newPage });

  // set results of pagination by Pagination component to this component's
  // state
  handlePaginatedRecordsChange = paginatedRecords =>
    this.setState({ paginatedRecords });

  filterBySearchQuery = (friends, fieldsSearched) => {
    const query = this.state.searchQuery;

    if (!friends) {
      return [];
    }
    if (query === "") {
      return friends || [];
    }

    return friends.filter(record =>
      fieldsSearched.some(fieldInfo => {
        const field = fieldInfo.getField(record);

        // directly regex compare fields if not array
        // else iterate over array then regex compare child elements
        return fieldInfo.type === FriendPage.fieldType.NON_ARRAY
          ? new RegExp(query, "i").test(field)
          : field.some(item =>
              new RegExp(query, "i").test(fieldInfo.getItem(item))
            );
      })
    );
  };

  filterByDropdowns = (friends, dropdownsInfo) => {
    if (!friends) {
      return [];
    }

    return friends.filter(record =>
      // eslint-disable-next-line no-confusing-arrow
      dropdownsInfo.every(dropdownInfo => {
        const currentState = this.state[dropdownInfo.stateName];

        return currentState === ""
          ? true
          : dropdownInfo.matcher(dropdownInfo.getField(record), currentState);
      })
    );
  };

  // removes saved friends if senior and removes not-friend friends
  filterFriends = friends => {
    let filteredFriends = friends;
    if (!friends) {
      return [];
    }
    filteredFriends = filteredFriends.filter(
      friend => friend.status !== "NOT_FRIEND"
    );
    if (this.props.isUserMaba) {
      return filteredFriends;
    }

    return filteredFriends.filter(friend => friend.status !== "SAVED");
  };

  capitalizeFirstLetter = string =>
    string.charAt(0).toUpperCase() + string.slice(1);

  // will receive filtered friend data and return FriendCards
  cardFactory = (data, isShowSenior) =>
    data.map(record =>
      this.state.activeTab === Tab.MY_FRIENDS ? (
        <FriendCard
          key={record.id}
          editable={false}
          friend={record}
          isDesktop={this.state.isDesktop}
          isShowSenior={isShowSenior}
          isUserMaba={isShowSenior}
          showActionButtons={true}
        />
      ) : (
        <FindFriendCard
          key={record.id}
          friend={record}
          isDesktop={this.state.isDesktop}
        />
      )
    );

  resize = () => {
    this.setState({ isDesktop: window.innerWidth >= 1024 });
  };

  // handler when tab clicked, tab only shown if is freshman account
  handleClickMyFriendTab = () =>
    this.setState({
      activeTab: Tab.MY_FRIENDS,
      searchQuery: "",
      currentPage: 1,
      statusSelection: "",
      majorSelection: "",
      classSelection: ""
    });

  handleClickFindFriendsTab = () => {
    this.props.fetchFindFriend("data");
    this.setState({
      activeTab: Tab.FIND_FRIENDS,
      searchQuery: "",
      currentPage: 1,
      statusSelection: "",
      majorSelection: "",
      classSelection: ""
    });
  };

  getStatisticFormatForStatisticsCard = () => {
    const statisticsFromFetch = this.props.statistic;
    const batchNames = ["quanta", "tarung", "omega", "elemen"];
    const statisticsInfo = {
      quanta: {
        approved: 0,
        total: 0,
        required: 0
      },
      omega: {
        approved: 0,
        total: 0,
        required: 0
      },
      tarung: {
        approved: 0,
        total: 0,
        required: 0
      },
      alumni: {
        approved: 0,
        total: 0,
        required: 0
      },
      mainRequirement: 0,
      totalApproved: 0,
      allTotal: 0
    };

    if (
      statisticsFromFetch === undefined ||
      Object.entries(statisticsFromFetch).length === 0
    ) {
      return statisticsInfo;
    }
    statisticsInfo.title = statisticsFromFetch.kenalan_task.title;
    statisticsInfo.mainRequirement =
      statisticsFromFetch.kenalan_task.required_all;
    statisticsInfo.totalApproved = statisticsFromFetch.kenalan_approved_all;
    statisticsInfo.allTotal = statisticsFromFetch.kenalan_created_all;

    batchNames.forEach(batchName => {
      const batchStatistic =
        statisticsInfo[batchName === "elemen" ? "alumni" : batchName];
      batchStatistic.approved =
        statisticsFromFetch[`kenalan_approved_${batchName}`];
      batchStatistic.total =
        statisticsFromFetch[`kenalan_created_${batchName}`];
      batchStatistic.required =
        statisticsFromFetch.kenalan_task[`required_${batchName}`];
    });

    return statisticsInfo;
  };

  testSearchQuery = event => {
    const query = event.target.value;
    this.setState({ searchInterestQuery: query });
  };

  submitFindFriendSearchQuery = () => {
    this.props.fetchFindFriend(this.state.searchQuery);
  };

  handleInterestSuggestionClick = event => {
    const query = event.target.getAttribute("data-interestName");
    if (this.state.activeTab === Tab.FIND_FRIENDS) {
      this.setState({
        searchQuery: query,
        interestSuggestions: []
      });
    }
  };

  showAllInterestSuggestion = () =>
    this.setState({ interestSuggestions: this.predefinedInterests });

  clearInterestSuggestion = () => this.setState({ interestSuggestions: [] });

  handleSearchbarFocus = () => {
    if (this.state.activeTab === Tab.FIND_FRIENDS) {
      this.showAllInterestSuggestion();
    }
  };

  handleSearchbarBlur = () => {
    if (this.state.activeTab === Tab.FIND_FRIENDS) {
      this.clearInterestSuggestion();
    }
  };

  render() {
    const filteredFriends = this.handleAnyFormChange();
    const statistic = this.getStatisticFormatForStatisticsCard();
    let friendCards = null;
    // if error
    if (this.props.isError === true) {
      friendCards = <EmptyCard text="Something went wrong" />;
      // if loading
    } else if (this.props.isLoadingFriendsWithInterests !== false) {
      friendCards = <Spinner />;
      // if have filtered friends
    } else if (this.state.paginatedRecords.length !== 0) {
      friendCards = this.cardFactory(
        filteredFriends.slice(
          (this.state.currentPage - 1) * FriendPage.numberOfFriendsPerPage,
          this.state.currentPage * FriendPage.numberOfFriendsPerPage
        ),
        this.props.isUserMaba
      );
      // if zero filtered friends
    } else {
      friendCards = <EmptyCard text="No friends found" />;
    }

    // includes searchbar, dropdowns and variable friendCards's content
    const friendSection = (
      <>
        <Section>
          <div className="FriendPage__search">
            <SearchBar
              placeholder={
                this.state.activeTab === Tab.MY_FRIENDS
                  ? "Search any name, title or interest"
                  : "Search any interest"
              }
              onQueryChange={this.getHandleChangeSearchbarFunction(
                "searchQuery"
              )}
              query={this.state.searchQuery}
              handleFocus={this.handleSearchbarFocus}
              handleBlur={this.handleSearchbarBlur}
            />
            {this.state.activeTab === Tab.FIND_FRIENDS && (
              <Buttons
                type="button"
                delete={!this.state.searchQuery}
                name={this.state.searchQuery ? "search" : "randomize"}
                onPressed={this.submitFindFriendSearchQuery}
              />
            )}
            {this.state.interestSuggestions.length > 0 && (
              <ul className="search-result">
                {this.state.interestSuggestions.map(interest => (
                  <li
                    className="search-result-item"
                    data-interestName={interest}
                    key={interest}
                    onMouseDown={this.handleInterestSuggestionClick}
                  >
                    {interest}
                  </li>
                ))}
              </ul>
            )}
          </div>
          <p className="FriendPage__subheading">Filter by</p>
          <div className="FriendPage__controls">
            <div className="FriendPage__controls__dropdowns">
              {this.state.activeTab === Tab.MY_FRIENDS
                ? this.dropdowns.map(info =>
                    info.isShow() ? (
                      <Dropdown
                        key={info.placeholder}
                        placeholder={info.placeholder}
                        anyAsOption={info.haveAnyOption}
                        anyOptionPlaceholder={info.anyOptionPlaceholder}
                        onDropdownChange={this.getHandleChangeFunction(
                          info.stateName
                        )}
                        options={info.options}
                      />
                    ) : (
                      <></>
                    )
                  )
                : this.dropdownsFindFriends.map(info =>
                    info.isShow() ? (
                      <Dropdown
                        key={`${info.placeholder}findfriends`}
                        placeholder={info.placeholder}
                        anyAsOption={info.haveAnyOption}
                        anyOptionPlaceholder={info.anyOptionPlaceholder}
                        onDropdownChange={this.getHandleChangeFunction(
                          info.stateName
                        )}
                        options={info.options}
                      />
                    ) : (
                      <></>
                    )
                  )}
            </div>
            <div className="FriendPage__controls__pagination">
              <Pagination
                records={filteredFriends}
                itemPerPage={FriendPage.numberOfFriendsPerPage}
                currentPage={this.state.currentPage}
                numberOfPagesShown={FriendPage.numberOfPageNumbersShown}
                onPageChange={nextPageNumber =>
                  this.handlePageChange(nextPageNumber)
                }
                onPaginatedRecordsChange={paginatedRecords =>
                  this.handlePaginatedRecordsChange(paginatedRecords)
                }
              />
            </div>
          </div>
        </Section>
        {friendCards}
      </>
    );

    return (
      <FriendPageStyle>
        <div className="FriendPage">
          {/* show statistics and tabs only if freshman */}
          {this.props.isUserMaba && (
            <>
              <Section title="Statistic">
                {this.props.isLoadingStatistic ? (
                  <Spinner />
                ) : (
                  <StatisticsCard {...statistic} />
                )}
              </Section>
              <Section>
                <div className="FriendPage__event-tab-container">
                  <div
                    className={`FriendPage__tab ${
                      this.state.activeTab === Tab.MY_FRIENDS
                        ? "FriendPage__tab__active"
                        : ""
                    }`}
                    onClick={this.handleClickMyFriendTab}
                  >
                    My Friends
                  </div>
                  <div
                    className={`FriendPage__tab ${
                      this.state.activeTab === Tab.FIND_FRIENDS
                        ? "FriendPage__tab__active"
                        : ""
                    }`}
                    onClick={this.handleClickFindFriendsTab}
                  >
                    Find Friends
                  </div>
                </div>
              </Section>
            </>
          )}
          <Section
            title={
              this.state.activeTab === Tab.MY_FRIENDS
                ? "my friends"
                : "find friends"
            }
          >
            {friendSection}
          </Section>
        </div>
      </FriendPageStyle>
    );
  }
}

FriendPage.propTypes = {
  fetchFriends: PropTypes.func.isRequired,
  fetchFindFriend: PropTypes.func,
  findFriends: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      user_detail: PropTypes.object,
      username: PropTypes.string
    })
  ),
  friends: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      profil: PropTypes.object
    })
  ),
  interests: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      interest_name: PropTypes.string,
      interest_category: PropTypes.string,
      is_top_interest: PropTypes.bool
    })
  ),
  friendsWithInterests: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      user_maba: PropTypes.object,
      user_elemen: PropTypes.shape({
        interests: PropTypes.array
      })
    })
  ),
  isLoadingFriendsWithInterests: PropTypes.bool,
  isError: PropTypes.bool,
  isUserMaba: PropTypes.bool,
  fetchStatisticKenalan: PropTypes.func.isRequired,
  isLoadingStatistic: PropTypes.bool,
  isLoadedStatistic: PropTypes.bool,
  statistic: PropTypes.object,
  patchedFriend: PropTypes.object,
  clearStatisticsIsLoaded: PropTypes.func
};

function mapStateToProps(state) {
  return {
    friends: getFriends(state),
    findFriends: getFindFriends(state),
    interests: getInterests(state),
    friendsWithInterests: getFriendsWithInterests(state),
    isLoadingFriendsWithInterests: getIsLoadingFriendsWithInterests(state),
    isError: getIsError(state),
    isUserMaba: state.profileReducer.get("isUserMaba"),
    statistic: getStatisticKenalan(state),
    isLoadingStatistic: isLoadingStatistic(state),
    isLoadedStatistic: isLoadedStatistic(state),
    isEditSuccess: isEditSuccess(state),
    patchedFriend: patchedFriend(state)
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchFriends: () => dispatch(fetchFriends()),
    fetchStatisticKenalan: () => dispatch(fetchStatisticKenalan()),
    editFriend: (id, description, status) =>
      dispatch(editFriend(id, description, status)),
    editApproveKenalan: (id, is_approved, rejection_message) =>
      dispatch(editApproveKenalan(id, is_approved, rejection_message)),
    clearStatisticsIsLoaded: () => dispatch(clearStatisticsIsLoaded()),
    fetchFindFriend: query => dispatch(fetchFindFriend(query))
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FriendPage);
