import React from "react";
import { FeedbackTabContainer } from "./style";
import FeedbackCard from "components/FeedbackCard";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { push } from "connected-react-router";
import { fetchFeedbacks } from "./actions";
import Loading from "components/Loading";
import EmptyCard from "components/EmptyCard";

class FeedbackTab extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      cardsToShow: 2,
      expanded: false
    };
    this.loadMore = this.loadMore.bind(this);
  }

  componentDidMount() {
    this.props.fetchFeedbacks();
  }

  loadMore(itemLength) {
    if (this.state.cardsToShow === 2) {
      this.setState({
        cardsToShow: itemLength,
        expanded: true
      });
    } else {
      this.setState({
        cardsToShow: 2,
        expanded: false
      });
    }
  }

  produceCards = objects =>
    objects.map((object, key) => <FeedbackCard key={key} feedback={object} />);

  render() {
    const isLoading = this.props.isLoading;
    const feedbacks = this.props.feedbacks;

    return isLoading ? (
      <Loading />
    ) : (
      <FeedbackTabContainer>
        {feedbacks.length !== 0 ? (
          this.produceCards(feedbacks.slice(0, this.state.cardsToShow))
        ) : (
          <EmptyCard text="There are no feedbacks yet! Come back later :D" />
        )}
        {feedbacks.length > 2 && !this.state.expanded && (
          <div className="loadMoreSection">
            <p onClick={() => this.loadMore(feedbacks.length)}>Load more...</p>
          </div>
        )}
      </FeedbackTabContainer>
    );
  }
}

FeedbackTab.propTypes = {
  fetchFeedbacks: PropTypes.func.isRequired,
  isLoading: PropTypes.bool,
  feedbacks: PropTypes.array
};

function mapStateToProps(state) {
  return {
    isLoading: state.feedbackTab.get("isLoading"),
    feedbacks: state.feedbackTab.get("feedbacks")
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchFeedbacks: () => dispatch(fetchFeedbacks()),
    push: url => dispatch(push(url))
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FeedbackTab);
