export const FETCH_EVENT_DETAIL = "src/EventDetailPage/FETCH_EVENT_DETAIL";
export const FETCH_EVENT_DETAIL_SUCCESS =
  "src/EventDetailPage/FETCH_EVENT_DETAIL_SUCCESS";
export const FETCH_EVENT_DETAIL_FAILED =
  "src/EventDetailPage/FETCH_EVENT_DETAIL_FAILED";
export const CLEAR_EVENT_DETAIL = "src/EventDetailPage/CLEAR_EVENT_DETAIL";

export const POST_QNA_COMMENT = "src/EventDetailPage/POST_QNA_COMMENT";
export const POST_QNA_COMMENT_SUCCESS =
  "src/EventDetailPage/POST_QNA_COMMENT_SUCCESS";
export const POST_QNA_COMMENT_FAILED =
  "src/EventDetailPage/POST_QNA_COMMENT_FAILED";
