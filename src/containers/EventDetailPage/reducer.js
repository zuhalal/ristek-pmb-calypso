/* eslint-disable no-case-declarations */
/* eslint-disable array-callback-return */
import { fromJS } from "immutable";
import {
  FETCH_EVENT_DETAIL,
  FETCH_EVENT_DETAIL_SUCCESS,
  FETCH_EVENT_DETAIL_FAILED,
  CLEAR_EVENT_DETAIL
} from "./constants";

const initialState = fromJS({
  event: {},
  error: null,
  isLoaded: false,
  isLoading: false
});

function eventDetailPageReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_EVENT_DETAIL:
      return state.set("isLoading", true);
    case FETCH_EVENT_DETAIL_SUCCESS:
      return state
        .set("event", action.payload)
        .set("isLoading", false)
        .set("isLoaded", true);
    case FETCH_EVENT_DETAIL_FAILED:
      return state
        .set("error", action.payload)
        .set("isLoading", false)
        .set("isLoaded", false);
    case CLEAR_EVENT_DETAIL:
      return state.set("error", null).set("isLoaded", false);
    default:
      return state;
  }
}

export default eventDetailPageReducer;
