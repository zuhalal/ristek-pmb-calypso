import axios from "axios";

import {
  FETCH_USER_INTEREST,
  FETCH_USER_INTEREST_FAILED,
  FETCH_USER_INTEREST_SUCCESS,
  POST_USER_INTEREST,
  POST_USER_INTEREST_FAILED,
  POST_USER_INTEREST_SUCCESS,
  DELETE_USER_INTEREST,
  DELETE_USER_INTEREST_FAILED,
  DELETE_USER_INTEREST_SUCCESS,
  PATCH_USER_INTEREST,
  PATCH_USER_INTEREST_FAILED,
  PATCH_USER_INTEREST_SUCCESS
} from "./constants";

import {
  interestApi,
  deleteInterestApi,
  getUserInterestApi,
  patchInterestApi
} from "api";
import swal from "sweetalert";

export function fetchUserInterest(id) {
  return dispatch => {
    dispatch({
      type: FETCH_USER_INTEREST
    });
    axios
      .get(getUserInterestApi(id))
      .then(response => {
        dispatch({
          payload: response.data,
          type: FETCH_USER_INTEREST_SUCCESS
        });
      })
      .catch(error => {
        dispatch({
          payload: error,
          type: FETCH_USER_INTEREST_FAILED
        });
      });
  };
}

export function postUserInterest(payload) {
  return dispatch => {
    dispatch({
      type: POST_USER_INTEREST
    });
    axios
      .post(interestApi, payload)
      .then(response => {
        dispatch({
          payload: response.data,
          type: POST_USER_INTEREST_SUCCESS
        });
      })
      .catch(error => {
        swal("Interest Add error", error, "error");
        dispatch({
          payload: error,
          type: POST_USER_INTEREST_FAILED
        });
      });
  };
}

export function deleteUserInterest(id) {
  return dispatch => {
    dispatch({
      type: DELETE_USER_INTEREST
    });
    axios
      .delete(deleteInterestApi(id))
      .then(response => {
        dispatch({
          payload: response,
          type: DELETE_USER_INTEREST_SUCCESS
        });
      })
      .catch(error => {
        swal("Interest delete error", error, "error");
        dispatch({
          payload: error,
          type: DELETE_USER_INTEREST_FAILED
        });
      });
  };
}

export function patchUserInterest(id) {
  return dispatch => {
    dispatch({
      type: PATCH_USER_INTEREST
    });
    axios
      .patch(patchInterestApi(id))
      .then(response => {
        dispatch({
          payload: response.data,
          type: PATCH_USER_INTEREST_SUCCESS
        });
      })
      .catch(error => {
        swal("move interest to top error", error, "error");
        dispatch({
          payload: error,
          type: PATCH_USER_INTEREST_FAILED
        });
      });
  };
}
