/* eslint-disable no-unused-expressions */
/* eslint-disable react/prop-types */
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { ProfilePageContainer } from "./style";
import ProfileTab from "../ProfileTab";
import FeedbackTab from "../FeedbackTab";
import StoryTab from "../StoryTab";
import { fetchProfile, fetchInterest } from "./actions";
import Loading from "components/Loading";
import { push } from "connected-react-router";
import { getUser } from "selectors/user";

class ProfilePage extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      key: "profile"
    };
    this.handleClick = this.handleClick.bind(this);
    this.renderProfileContent = this.renderProfileContent.bind(this);
  }

  componentDidMount() {
    this.props.fetchProfile();
    const userId = this.props.user.user_id;
    this.props.fetchInterest(userId);
    if (this.props.location.state && this.props.location.state.key) {
      this.setState({
        key: this.props.location.state.key
      });
    }
    this.props.location.search === "?=feedback" &&
      this.setState({ key: "feedback" });

    this.props.location.search === "?=story" && this.setState({ key: "story" });
  }

  handleClick(toggledButton) {
    switch (toggledButton.toLowerCase()) {
      case "profile":
        this.setState({
          key: "profile"
        });
        break;
      case "feedback":
        this.setState({
          key: "feedback"
        });
        break;
      case "story":
        this.setState({
          key: "story"
        });
        break;
      default:
        break;
    }
  }

  renderProfileContent(profile) {
    if (this.state.key === "profile") {
      return (
        <ProfileTab
          profile={profile}
          interests={this.props.interest}
          push={this.props.push}
        />
      );
    } else if (this.state.key === "feedback") {
      return <FeedbackTab />;
    } else if (this.state.key === "story") {
      return <StoryTab profile={profile} push={this.props.push} />;
    }

    return <div />;
  }

  render() {
    const profil = this.props.profil;
    const title = this.props.title;
    const kelompok = this.props.kelompok;
    const isLoading = this.props.isLoading;
    const isUserMaba = this.props.isUserMaba;
    const angkatan = this.props.angkatan;

    const kel = kelompok ? kelompok.value : null;

    const profile = {
      profileImage: profil.get("foto"),
      name: profil.get("nama_lengkap"),
      position: isUserMaba ? kel : title,
      major: `Fasilkom ${angkatan.get("tahun")}`,
      highSchool: profil.get("asal_sekolah"),
      placeOfBirth: profil.get("tempat_lahir"),
      dateOfBirth: profil.get("tanggal_lahir"),
      email: profil.get("email"),
      line: profil.get("line_id"),
      linkedin: profil.get("linkedin"),
      isPrivate: profil.get("is_private")
    };

    return isLoading ? (
      <Loading />
    ) : (
      <ProfilePageContainer>
        <div className="event-tab-container">
          <div
            className={`profile tab ${
              this.state.key === "profile" ? "active" : ""
            }`}
            onClick={() => this.handleClick("profile")}
          >
            Profile
          </div>
          {isUserMaba && (
            <div
              className={`feedback tab ${
                this.state.key === "feedback" ? "active" : ""
              }`}
              onClick={() => this.handleClick("feedback")}
            >
              Feedback
            </div>
          )}
          <div
            className={`story tab ${
              this.state.key === "story" ? "active" : ""
            }`}
            onClick={() => this.handleClick("story")}
          >
            Story
          </div>
        </div>
        {this.renderProfileContent(profile)}
      </ProfilePageContainer>
    );
  }
}

// <Tabs
//   id="controlled-tab-example"
//   activeKey={this.state.key}
//   onSelect={key => this.setState({ key })}
// >
//   <Tab eventKey="profile" title="Profile">
//     <ProfileTab profile={profile} push={this.props.push} />
//   </Tab>
// </Tabs>
// <Tab eventKey="feedback" title="Feedback">
//   <FeedbackTab />
// </Tab>
// <Tab eventKey="story" title="Story">
//   <StoryTab />
// </Tab>

ProfilePage.propTypes = {
  fetchProfile: PropTypes.func.isRequired,
  fetchInterest: PropTypes.func.isRequired,
  isLoading: PropTypes.bool,
  isUserMaba: PropTypes.bool,
  profil: PropTypes.object.isRequired,
  title: PropTypes.string,
  kelompok: PropTypes.string,
  angkatan: PropTypes.object.isRequired,
  push: PropTypes.func,
  user: PropTypes.func.isRequired,
  interest: PropTypes.object,
  location: PropTypes.shape()
};

function mapStateToProps(state) {
  return {
    isLoading: state.profileReducer.get("isLoading"),
    isUserMaba: state.profileReducer.get("isUserMaba"),
    profil: state.profileReducer.get("profil"),
    title: state.profileReducer.get("title"),
    kelompok: state.profileReducer.get("kelompok"),
    angkatan: state.profileReducer.get("angkatan"),
    interest: state.profileReducer.get("interest"),
    user: getUser(state)
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchProfile: () => dispatch(fetchProfile()),
    fetchInterest: id => dispatch(fetchInterest(id)),
    push: url => dispatch(push(url))
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfilePage);
