import { fromJS } from "immutable";
import { getNamaAngkatan } from "utils/general";
import {
  FETCH_PROFILE,
  FETCH_PROFILE_SUCCESS,
  FETCH_PROFILE_FAILED,
  UPDATE_PROFILE,
  FETCH_INTEREST,
  FETCH_INTEREST_SUCCESS,
  FETCH_INTEREST_FAILED
} from "./constants";

const initialState = fromJS({
  error: null,
  isLoading: false,
  isUserMaba: false,
  profilId: null,
  profil: {},
  title: null,
  kelompok: {},
  angkatan: {},
  interest: {}
});

function profilePageReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_PROFILE:
      return state.set("isLoading", true);
    case FETCH_PROFILE_SUCCESS:
      return state
        .set("profilId", action.payload.id)
        .set("profil", fromJS(action.payload.profil))
        .set(
          "title",
          action.payload.title
            ? action.payload.title
            : `${getNamaAngkatan(action.payload.profil.angkatan.tahun)} ${
                action.payload.profil.angkatan.tahun
              }`
        )
        .set(
          "kelompok",
          action.payload.kelompok
            ? {
                id: action.payload.kelompok.id,
                value: `${action.payload.kelompok.kelompok_besar.nama} -
              ${action.payload.kelompok.kelompok_kecil.nama}`
              }
            : {}
        )
        .set("angkatan", fromJS(action.payload.profil.angkatan))
        .set("isLoading", false)
        .set("isUserMaba", action.payload.profil.angkatan.tahun === "2019");
    case FETCH_PROFILE_FAILED:
      return state.set("error", action.payload).set("isLoading", false);
    case UPDATE_PROFILE:
      return state
        .set("profilId", action.payload.id)
        .set("profil", fromJS(action.payload.profil))
        .set(
          "title",
          action.payload.title
            ? action.payload.title
            : `Fasilkom ${action.payload.profil.angkatan.tahun}`
        )
        .set(
          "kelompok",
          action.payload.kelompok
            ? {
                id: action.payload.kelompok.id,
                value: `${action.payload.kelompok.kelompok_besar.nama} -
              ${action.payload.kelompok.kelompok_kecil.nama}`
              }
            : {}
        )
        .set("angkatan", fromJS(action.payload.profil.angkatan))
        .set("isUserMaba", action.payload.profil.angkatan.tahun === "2019");
    case FETCH_INTEREST:
      return state.set("isLoading", true);
    case FETCH_INTEREST_SUCCESS:
      return state.set("interest", action.payload).set("isLoading", false);
    case FETCH_INTEREST_FAILED:
      return state.set("error", action.payload).set("isLoading", false);
    default:
      return state;
  }
}

export default profilePageReducer;
