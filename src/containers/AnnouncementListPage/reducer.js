import { fromJS } from "immutable";
import {
  FETCH_ANNOUNCEMENTS,
  FETCH_ANNOUNCEMENTS_SUCCESS,
  FETCH_ANNOUNCEMENTS_FAILED
} from "./constants";

const initialState = fromJS({
  announcements: [],
  error: null,
  isLoaded: false,
  isLoading: false
});

function announcementReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_ANNOUNCEMENTS:
      return state.set("isLoading", true);
    case FETCH_ANNOUNCEMENTS_SUCCESS:
      return state
        .set("announcements", action.payload)
        .set("isLoading", false)
        .set("isLoaded, true");
    case FETCH_ANNOUNCEMENTS_FAILED:
      return state
        .set("error", action.payload)
        .set("isLoading", false)
        .set("isLoaded", false);
    default:
      return state;
  }
}

export default announcementReducer;
