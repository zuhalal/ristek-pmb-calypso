export const FETCH_FRIEND = "src/FormFriendPage/FETCH_FRIEND";

export const FETCH_FRIEND_SUCCESS = "src/FormFriendPage/FETCH_FRIEND_SUCCESS";

export const FETCH_FRIEND_FAILED = "src/FormFriendPage/FETCH_FRIEND_FAILED";

export const FETCH_INTEREST = "src/FormFriendPage/FETCH_INTEREST";

export const FETCH_INTEREST_SUCCESS =
  "src/FormFriendPage/FETCH_INTEREST_SUCCESS";

export const FETCH_INTEREST_FAILED = "src/FormFriendPage/FETCH_INTEREST_FAILED";

export const PATCH_FRIEND = "src/FormFriendPage/PATCH_FRIEND";
export const PATCH_FRIEND_SUCCESS = "src/FormFriendPage/PATCH_FRIEND_SUCCESS";
export const PATCH_FRIEND_FAILED = "src/FormFriendPage/PATCH_FRIEND_FAILED";

export const PATCH_FRIEND_MABA = "src/FormFriendPage/PATCH_FRIEND_MABA";
export const PATCH_FRIEND_SUCCESS_MABA =
  "src/FormFriendPage/PATCH_FRIEND_SUCCESS_MABA";
export const PATCH_FRIEND_FAILED_MABA =
  "src/FormFriendPage/PATCH_FRIEND_FAILED_MABA";

export const FETCH_SAVED_FRIENDS_COUNT =
  "src/FormFriendPage/FETCH_SAVED_FRIENDS_COUNT";
export const FETCH_SAVED_FRIENDS_COUNT_SUCCESS =
  "src/FormFriendPage/FETCH_SAVED_FRIENDS_COUNT_SUCCES";
export const FETCH_SAVED_FRIENDS_COUNT_FAILED =
  "src/FormFriendPage/FETCH_SAVED_FRIENDS_COUNT_FAILED";

export const CLEAR_SUBMIT_SUCCESS = "src/FormFriendPage/CLEAR_SUBMIT_SUCCESS";
export const CLEAR_SAVED_FRIENDS_COUNT_IS_LOADED =
  "src/FormFriendPage/CLEAR_SAVED_FRIENDS_COUNT_IS_LOADED";
