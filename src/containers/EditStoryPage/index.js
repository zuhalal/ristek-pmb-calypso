import React from "react";
import { EditStoryPageContainer } from "./style";
import Card from "../../components/Card";
import { Buttons } from "components/Buttons";
import { editStory, resetState } from "./actions";
import PropTypes from "prop-types";
import { push } from "connected-react-router";
import { connect } from "react-redux";
import Loading from "components/Loading";
import Button from "components/Button";

class EditStoryPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: null,
      link: null
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    if (!this.props.location.state || !this.props.location.state.link) {
      this.props.push("/hayo-nyoba-apa");
    } else {
      this.setState({
        link: this.props.location.state.link.replace("https://medium.com/", ""),
        id: this.getParam()
      });
    }
  }

  getParam = () => {
    const params = this.props.location.pathname.split("/")[3];

    return params;
  };

  handleChange(event) {
    const value = event.target.value;

    this.setState({ link: value });
  }

  handleSubmit(event) {
    event.preventDefault();
    const obj = {
      link: `https://medium.com/${this.state.link}`
    };

    this.props.editStory(obj, this.state.id);
  }

  componentDidUpdate() {
    if (this.props.isSuccess) {
      // this.props.history.goBack();
      this.props.resetState();
      this.props.push({
        pathname: "/profile",
        state: { key: "story" }
      });
    }
  }

  render() {
    return this.props.isLoading ? (
      <Loading />
    ) : (
      <EditStoryPageContainer>
        <div className="back-button-wrap">
          <Button
            text="<<< Back"
            onClick={() => this.props.push("/profile?=story")}
          />
        </div>
        <h1>Lets Share Your Story to Our New Family!</h1>
        <Card>
          <h2>Fill the information below</h2>
          <form onSubmit={this.handleSubmit}>
            <div className="formSection">
              <label>Medium URL</label>
              <div className="urlsection">
                <div className="leftSection">
                  <small>https://medium.com/</small>
                </div>
                <div className="rightSection">
                  <input onChange={this.handleChange} value={this.state.link} />
                </div>
              </div>
              <div className="submitButton">
                <Buttons name="Save" type="submit" wide="8rem" />
              </div>
            </div>
          </form>
        </Card>
      </EditStoryPageContainer>
    );
  }
}

function mapStateToProps(state) {
  return {
    link: state.editStoryReducer.get("link"),
    isLoading: state.editStoryReducer.get("isLoading"),
    isSuccess: state.editStoryReducer.get("isSuccess")
  };
}

function mapDispatchToProps(dispatch) {
  return {
    editStory: (obj, id) => dispatch(editStory(obj, id)),
    resetState: () => dispatch(resetState()),
    push: url => dispatch(push(url))
  };
}

EditStoryPage.propTypes = {
  link: PropTypes.object.isRequired,
  editStory: PropTypes.func.isRequired,
  isSuccess: PropTypes.bool.isRequired,
  push: PropTypes.func.isRequired,
  isLoading: PropTypes.bool,
  location: PropTypes.shape().isRequired,
  resetState: PropTypes.func.isRequired
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditStoryPage);
