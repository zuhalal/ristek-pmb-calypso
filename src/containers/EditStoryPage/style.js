import styled from "styled-components";

export const EditStoryPageContainer = styled.div`
  @import url("https://fonts.googleapis.com/css?family=Rubik:400,500,700&display=swap");
  font-family: Rubik;

  .formSection {
    display: flex;
    flex-direction: column;
    padding: 1rem;
  }

  .urlsection {
    display: flex;
    flex-direction: row;
    margin: 0 0 1rem 1rem;
  }

  .urlsection .leftSection {
    margin-right: 0.5rem;
  }

  .urlsection .rightSection input {
    height: 1rem;
    font-size: 12px;
  }

  .leftSection {
    padding-top: 0.25rem;
  }

  .rightSection {
    width: 100%;
  }

  label {
    width: 100%;
    margin-bottom: 1rem;
  }

  label p {
    margin-bottom: 0.4rem;
    padding-top: 5rem;
  }

  label.uploadLabel {
    margin-bottom: 0;
  }

  input {
    border: 1px solid ${props => props.theme.colors.lightGray};
    border-radius: 5px;
    height: 1.25rem;
    background-color: ${props => props.theme.colors.fieldGray};
    outline: none;
    padding: 5px;
    margin: 0;
    width: 100%;
  }

  input:focus {
    background-color: ${props => props.theme.colors.white};
  }

  .submitButton {
    display: flex;
    align-self: flex-end;
    justify-content: flex-end;
  }

`;
