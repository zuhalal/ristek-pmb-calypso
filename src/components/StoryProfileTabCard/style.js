import styled from "styled-components";

export const StoryProfileTabCardContainer = styled.div`
  .titleSection {
    width: 100%;
    margin-bottom: 1rem;
  }

  .titleSection h1 {
    margin: 0rem;
    color: ${props => props.theme.colors.primaryBlack};
  }

  .profileSection {
    display: flex;
    flex-direction: row;
    width: 100%;
  }

  .modifySection {
    display: flex;
    flex-direction: row;
    align-items: flex-start;
  }

  .editSection {
    cursor: pointer;
  }

  .deleteSection {
    cursor: pointer;
  }

  .modifySection img {
    margin: 0.75rem 0.5rem;
    width: 1.25rem;
  }

  .blog-link {
    color: ${props => props.theme.colors.gray};
  }

  .imageSection {
    width: 4rem;
    height: 4rem;
  }

  .feedImage {
    height: 20rem;
    object-fit: none;
    margin: 0;
  }

  .content {
    display: flex;
    flex-direction: column;
  }

  .profileContent {
    padding-top: 1rem;
    width: 100%;
  }

  .desc {
    padding-top: 1rem;
    padding-bottom: 0.5rem;
    color: ${props => props.theme.colors.gray};
  }

  .position {
    color: ${props => props.theme.colors.gray};
  }

  .time {
    color: ${props => props.theme.colors.gray};
    font-size: 0.75rem;
  }

  .footer {
    padding-top: 1rem;
  }

  .nama {
    font-family: Rubik;
    font-style: normal;
    font-weight: 500;
    font-size: 1.25rem;
  }

  @media (max-width: 1216px) {
  }

  @media (max-width: 1024px) {
  }

  @media (max-width: 580px) {
  }

  @media (max-width: 480px) {
    .titleSection h1 {
      font-size: 20px;
    }

    .profileContent .nama {
      font-size: 15px;
    }

    .imageSection {
      width: 3rem;
      height: 3rem;
      margin-right: 0.75rem;
    }

    .feedImage {
      height: 10rem;
    }

    .content .desc {
      font-size: 12px;
    }

    .modifySection img {
      width: 1rem;
    }
  }

  @media (max-width: 350px) {
  }
`;
