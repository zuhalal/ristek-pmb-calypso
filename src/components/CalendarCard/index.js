import React, { Component } from "react";

import Card from "../Card";
import { CalendarCardStyle, TooltipStyle } from "./style";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { isEqual } from "lodash";

import Calendar from "react-calendar";
import moment from "moment-timezone";

// uses npm package https://www.npmjs.com/package/react-calendar
class CalendarCard extends Component {
  shouldComponentUpdate(nextProps) {
    // only update when date changes
    if (
      !moment(this.props.serverTime)
        .startOf("day")
        .isSame(moment(nextProps.serverTime).startOf("day")) ||
      !isEqual(this.props.events, nextProps.events)
    ) {
      return true;
    }

    return false;
  }

  // month year format at top of calendar
  formatDate = date =>
    // eslint-disable-next-line object-property-newline
    date.toLocaleDateString("default", { month: "long", year: "numeric" });

  // format event time for tooltip
  formatTime = date =>
    // eslint-disable-next-line object-property-newline
    date.toLocaleTimeString("default", {
      hour: "2-digit",
      minute: "2-digit",
      hour12: false
    });

  render() {
    return (
      <CalendarCardStyle>
        <Card>
          <div className="CalendarCard__title">PMB Calendar</div>
          <div id="Calendar">
            <Calendar
              value={new Date(this.props.serverTime)}
              minDetail="month"
              maxDetail="month"
              showNeighboringMonth={false}
              formatMonthYear={(locale, date) => this.formatDate(date)}
              tileContent={tile => {
                const events = this.props.events[moment(tile.date).unix()];

                return events === undefined ? (
                  <></>
                ) : (
                  <Tooltip
                    message={
                      <div className="Calendar__Tooltip__messages">
                        {events.map(event => (
                          <a key={event.title} href={event.link}>
                            [{this.formatTime(new Date(event.date))}]{" "}
                            {event.title}
                          </a>
                        ))}
                      </div>
                    }
                    position="top"
                  >
                    <div className="Calendar__tileContent" />
                  </Tooltip>
                );
              }}
              tileClassName={tile => {
                const eventsDetail = this.props.events[
                  moment(tile.date).unix()
                ];

                return eventsDetail === undefined
                  ? "CalendarCard__tile"
                  : "CalendarCard__tile CalendarCard__tile__event";
              }}
            />
          </div>
        </Card>
      </CalendarCardStyle>
    );
  }
}

/*
  Tooltip not extracted to separate component, due to heavy customization
  and workarouds to work specifically with calendar

  https://codepen.io/andrewerrico/pen/OjbvvW?editors=0110
*/
class Tooltip extends Component {
  constructor(props) {
    super(props);
    this.state = {
      displayTooltip: false
    };

    this.hideTooltip = this.hideTooltip.bind(this);
    this.showTooltip = this.showTooltip.bind(this);
  }

  hideTooltip() {
    this.setState({ displayTooltip: false });
  }

  showTooltip() {
    this.setState({ displayTooltip: true });
  }

  render() {
    const message = this.props.message;

    return (
      <TooltipStyle>
        <div className="Tooltip" onMouseLeave={this.hideTooltip}>
          {this.state.displayTooltip && (
            <div className="Tooltip__bubble Tooltip__top">
              <div className="Tooltip__message">{message}</div>
            </div>
          )}
          <div className="Tooltip__trigger" onMouseOver={this.showTooltip}>
            {this.props.children}
          </div>
        </div>
      </TooltipStyle>
    );
  }
}

CalendarCard.propTypes = {
  serverTime: PropTypes.string,
  events: PropTypes.objectOf(
    PropTypes.arrayOf(
      PropTypes.shape({
        title: PropTypes.string,
        date: PropTypes.string,
        link: PropTypes.string
      })
    )
  ).isRequired
};

Tooltip.propTypes = {
  children: PropTypes.node.isRequired,
  message: PropTypes.node.isRequired,
  position: PropTypes.string.isRequired
};

function mapStateToProps(state) {
  return {
    serverTime: state.global.get("serverTime")
  };
}

export default connect(
  mapStateToProps,
  null
)(CalendarCard);
