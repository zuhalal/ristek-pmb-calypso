/* eslint-disable consistent-return */
/* eslint-disable no-param-reassign */
import React, { useState } from "react";
import TokenGeneratorContainer from "./style";
import PropTypes from "prop-types";
import swal from "sweetalert";
import Card from "components/Card";

const TokenGenerator = ({ onGenerate }) => {
  const [limit, setLimit] = useState(1);

  function increment(add) {
    let currentLimit = limit;
    if (!limit) {
      currentLimit = 0;
    }
    let total = currentLimit + add;
    if (total < 1) {
      total = 1;
    } else if (total > 15) {
      total = 15;
    }

    setLimit(total);
  }

  function handleChange(number) {
    if (number > 15) {
      number = 15;
    }
    setLimit(number);
  }

  function handleGenerate() {
    if (limit === 0 || !limit) {
      return swal(
        "Error",
        "Please input number in range 1 - 15 to generate token",
        "error"
      );
    }

    onGenerate(limit);
  }

  return (
    <TokenGeneratorContainer>
      <Card>
        <div className="body-cont">
          <p className="desc-text">Insert the number of your new friends:</p>
          <div className="input-cont">
            <div className="button" onClick={() => increment(-1)}>
              <p>-</p>
            </div>
            <input
              className="input-field"
              type="number"
              value={limit}
              onChange={evt => handleChange(evt.target.value)}
            />
            <div className="button" onClick={() => increment(1)}>
              <p>+</p>
            </div>
          </div>
          <div className="submit-btn" onClick={() => handleGenerate()}>
            Generate Token
          </div>
        </div>
      </Card>
    </TokenGeneratorContainer>
  );
};

TokenGenerator.propTypes = {
  onGenerate: PropTypes.func.isRequired
};

export default TokenGenerator;
