import React, { Component } from "react";
import PropTypes from "prop-types";

import { SectionStyle } from "./style";

class Section extends Component {
  render() {
    const title = this.props.title;

    return (
      <SectionStyle>
        <div className="Section">
          {title ? <div className="Section__title">{title}</div> : ""}
          <div className="Section__content">{this.props.children}</div>
        </div>
      </SectionStyle>
    );
  }
}

Section.propTypes = {
  title: PropTypes.string,
  children: PropTypes.node.isRequired
};

export default Section;
