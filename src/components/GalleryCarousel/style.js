import styled from "styled-components";

export const GalleryCarouselStyle = styled.div`
  .GalleryCarousel {
    display: flex;
    align-items: center;
    justify-content: center;
    position: fixed;
    box-sizing: border-box;
    top: 0;
    left: 0;
    width: 100vw;
    height: 100vh;
    padding: 0 0.5rem;
    background: rgba(0, 0, 0, ${props => props.darkenPercentage});
    z-index: 10;

    @media (min-width: ${props => props.theme.sizes.desktop}) {
      padding: 0 2rem;
    }
  }

  .GalleryCarousel__content {
    display: flex;
    flex-direction: column;
    align-items: center;
    flex-grow: 1;
    max-width: ${props => props.theme.sizes.fullhd};
    max-height: 800px;
    height: 100%;
  }

  .GalleryCarousel__carousel {
    display: flex;
    width: 100%;
    align-items: center;
    justify-content: center;
    flex-grow: 1;
  }

  .GalleryCarousel__carousel__slider {
    position: relative;
    width: 100%;
    height: calc(100% - 4rem);
    overflow: hidden;
    white-space: nowrap;

    @media (min-width: ${props => props.theme.sizes.desktop}) {
      width: calc(100% - 2rem);
      height: calc(100% - 6rem);
    }

    @media screen and (min-width: ${props => props.theme.sizes.widescreen}) {
      width: calc(100% - 4rem);
    }

    @media screen and (min-width: ${props => props.theme.sizes.fullhd}) {
      width: calc(100% - 8rem);
    }
  }

  .GalleryCarousel__carousel__slider-wrapper {
    position: relative;
    height: 100%;
    width: 100%;
    transform: translateX(${props => props.translateValue}px);
    ${props => props.isPlayAnimation && "transition: transform ease-out 0.6s;"}
  }

  .GalleryCarousel__menu {
    display: flex;
    justify-content: center;
    width: calc((5 * 100px) + (4 * 2rem));
    height: 100px;
    margin-bottom: 2rem;

    @media screen and (max-height: 500px) {
      display: none;
    }
  }

  .GalleryCarousel__menu__slider {
    position: relative;
    width: 100%;
    height: 100%;
    overflow: hidden;
    white-space: nowrap;
  }

  .GalleryCarousel__menu__slider-wrapper {
    position: relative;
    height: 100%;
    width: 100%;
    transform: translateX(${props => props.menuTranslateValue}px);
    ${props => props.isPlayAnimation && "transition: transform ease-out 0.6s;"}
  }

  .GalleryCarousel__button-right,
  .GalleryCarousel__button-left,
  .GalleryCarousel__button-close {
    width: 45px;
    height: 45px;
    background-repeat: no-repeat;
    background-position: center center;
    cursor: pointer;

    @media (min-width: ${props => props.theme.sizes.desktop}) {
      width: 60px;
      height: 60px;
    }

    @media screen and (min-width: ${props => props.theme.sizes.widescreen}) {
      width: 90px;
      height: 90px;
    }
  }

  .GalleryCarousel__button-right,
  .GalleryCarousel__button-left {
    background-image: url(${props => props.nextButtonImage});
    background-size: 30px;

    @media (min-width: ${props => props.theme.sizes.desktop}) {
      background-size: 40px;
    }

    @media screen and (min-width: ${props => props.theme.sizes.widescreen}) {
      background-size: 60px;
    }
  }

  .GalleryCarousel__button-left {
    transform: scaleX(-1);
    margin-right: 1.5rem;
  }

  .GalleryCarousel__button-right {
    margin-left: 1.5rem;
  }

  .GalleryCarousel__button-close {
    position: absolute;
    top: 0.5rem;
    right: 0.5rem;
    background-image: url(${props => props.closeButtonImage});
    background-size: 20px;

    @media (min-width: ${props => props.theme.sizes.desktop}) {
      top: 2rem;
      right: 2rem;
      background-size: 27px;
    }

    @media screen and (min-width: ${props => props.theme.sizes.widescreen}) {
      background-size: 40px;
    }
  }

  .GalleryCarousel__hide {
    display: none;
  }
`;

export const GalleryCarouselImageStyle = styled.div`
  &.GalleryCarouselImage__carousel-image,
  &.GalleryCarouselMenuImage__menu-image {
    display: inline-block;
    background-image: url(${props => props.image});
    background-position: center center;
    background-repeat: no-repeat;
    cursor: pointer;
  }

  &.GalleryCarouselImage__carousel-image {
    height: 100%;
    width: 100%;
    background-size: contain;
  }

  &.GalleryCarouselMenuImage__menu-image {
    width: 100px;
    height: 100px;
    background-size: cover;
    margin-right: 2rem;

    &:first-child {
      margin-left: calc(2 * 100px + 2 * 2rem);
    }

    &:last-child {
      margin-right: 0;
    }
  }
`;
