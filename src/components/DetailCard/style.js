import styled from "styled-components";

export const EventDetailCard = styled.div`
  p,
  h1 {
    margin: 0;
  }

  .EventCard__card__content > * {
    margin-bottom: 0.5rem;

    &:last-child {
      margin-bottom: 0;
    }
  }

  .EventCard .Card {
    padding: 0;

    .back-button-wrap {
      padding: 1rem 1rem 0;
    }
  }

  .EventCard__card__content {
    padding: 1rem;

    p {
      font-size: 14px;
    }

    h1 {
      font-size: 24px;
      color: ${props => props.theme.colors.blue};
    }

    .row {
      display: flex;
      flex-direction: row;
      justify-content: space-between;
      align-items: center;
    }
  }

  .logoset-container {
    display: flex;
    flex-direction: row;
    background: ${props => props.theme.colors.blue};
    color: ${props => props.theme.colors.white};
    border-radius: 7px;

    .logoset-description {
      font-size: 14px;
      padding: 0 5px;
      display: flex;
      flex-direction: column;
      justify-content: center;
    }
  }

  .EventCard__card__image {
    border-radius: 5px;
    margin-top: 1.2rem;
    box-shadow: ${props => props.theme.boxShadow};
    /* make div width height ratio 3:1 */
    padding-top: 33.3%;
    background-color: gray;
    background-image: url(${props => props.image});
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center center;
  }

  .EventCard__card__logo {
    height: 30px;
    width: 30px;
    background-color: white;
    background-image: url(${props => props.logo});
    background-repeat: no-repeat;
    background-position: center center;
    background-size: contain;
  }

  .info {
    font-weight: ${props => props.theme.weight.bold};
  }

  .EventCard__card__footer {
    display: flex;
    justify-content: flex-start;
    flex-direction: column;
    padding: 1rem;

    .attachment-container {
      margin-bottom: 1rem;
      display: flex;
      align-items: center;

      img {
        margin-right: 0.5rem;
      }

      a {
        cursor: pointer;
        font-weight: ${props => props.theme.weight.bold};
        font-size: 14px;
        color: ${props => props.theme.colors.blue};
      }

      a:hover {
        color: blue;
      }
    }
  }

  .EventCard__card__date {
    display: flex;
    flex-direction: row;
    align-items: center;
    color: ${props => props.theme.colors.primaryBlack};

    .EventCard__card__date__icon {
      margin-right: 0.5rem;
    }
  }

  .EventCard__card__location {
    display: flex;
    flex-direction: row;
    align-items: center;
    color: ${props => props.theme.colors.primaryBlack};

    .EventCard__card__location__icon {
      margin-right: 0.5rem;
      height: 24px;
    }
  }

  .EventCard__card__text {
    margin: 1rem 0;
  }

  .mobile {
    display: none;
  }

  .gray {
    color: ${props => props.theme.colors.gray};
  }

  @media (max-width: ${props => props.theme.sizes.desktop}) {
    p {
      font-size: 12px;
    }

    .mobile {
      display: flex;
    }

    .EventCard__card__footer {
      justify-content: space-between;
    }

    .EventCard__card__logo {
      height: 27px;
      width: 28px;
    }

    .logoset-container {
      align-self: flex-end;
    }

    .desktop {
      display: none;
    }
  }
`;
