import styled from "styled-components";

export const WhiteCardComponents = styled.div`
  .white-card {
    background-color: ${props => props.theme.colors.white};
    width: 100%;
    height: auto;
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
    margin: 3rem 0;
    padding: 1rem;
  }

  @media screen and (max-width: 1024px) {
    .white-card {
      width: auto;
      border-radius: 8px;
    }
  }
`;
