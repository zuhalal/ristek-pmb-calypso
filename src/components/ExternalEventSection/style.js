import styled from "styled-components";

export const ExternalEventSectionContainer = styled.div`
  .event-wrap {
    margin: 1rem 0;
  }

  .title {
      font-size: 20px;
    }
  }

  @media (max-width: 600px) {
    .title {
      font-size: 16px;
    }
  }
`;
