import styled from "styled-components";

export const StoryListCardStyle = styled.div`
  p,
  h3 {
    margin: 0;
  }

  .StoryListCard .Card {
    padding: 0;
  }

  .StoryListCard__card__content {
    padding: 1rem;
  }

  .StoryListCard__card__content > * {
    margin-bottom: 0.5rem;

    &:last-child {
      margin-bottom: 0;
    }
  }

  .StoryListCard__card__content > p {
    font-size: 12px;
  }

  .StoryListCard__card__content > h3 {
    font-size: 16px;
  }

  .StoryListCard__card__image {
    border-radius: 5px;
    box-shadow: ${props => props.theme.boxShadow};
    /* make div width height ratio 3:1 */
    padding-top: 33.3%;
    background-color: gray;
    background-image: url(${props => props.image});
    background-repeat: no-repeat;
    background-position: center center;
    background-size: cover;
  }
`;
