/* eslint-disable global-require */
import React, { Component } from "react";
import PropTypes from "prop-types";

import Card from "../Card";
import { StoryListCardStyle } from "./style";

// to ensure image rendered correctly, use image with 3:1 ratio
class StoryListCard extends Component {
  formatDate = date =>
    date.toLocaleDateString("default", {
      day: "numeric",
      month: "long",
      year: "numeric"
    });

  render() {
    const title = this.props.title;
    const date = this.formatDate(this.props.date);
    const description = this.props.description;
    const imageUrl = this.props.imageUrl;

    return (
      <StoryListCardStyle image={imageUrl}>
        <div className="StoryListCard">
          <Card>
            <div className="StoryListCard__card">
              {this.props.imageUrl ? (
                <div className="StoryListCard__card__image" />
              ) : (
                ""
              )}
              <div className="StoryListCard__card__content">
                <p>{date}</p>
                <h3>{title}</h3>
                <p>{description}</p>
              </div>
            </div>
          </Card>
        </div>
      </StoryListCardStyle>
    );
  }
}

StoryListCard.propTypes = {
  title: PropTypes.string.isRequired,
  date: PropTypes.instanceOf(Date).isRequired,
  imageUrl: PropTypes.string,
  description: PropTypes.string.isRequired
};

export default StoryListCard;
