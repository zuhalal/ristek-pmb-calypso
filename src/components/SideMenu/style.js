import styled from "styled-components";

export const SideMenuStyle = styled.div`
  @import url("https://fonts.googleapis.com/css?family=Rubik:400,500,700&display=swap");
  display: flex;
  flex-grow: 1;

  .sidemenu {
    width: 100vw;
    background: ${props => props.theme.colors.boneWhite};
    min-height: 100%;
    height: 100%;
    box-shadow: 2px 0px 5px rgba(0, 0, 0, 0.5);
    position: fixed;
    top: 0;
    left: 0;
    z-index: 200;
    transform: translateX(-100%);
    transition: transform 0.5s ease-out;
    display: flex;
    flex-direction: column;
    overflow-y: auto;
  }

  .sidemenu.open {
    transform: translateX(0);
  }

  .pmb-logo-container {
    display: flex;
    width: 100%;
    justify-content: center;
    margin: 2rem 0 0 0;
  }

  .pmb__wrapper {
    align-self: center;

    img {
      height: auto;
      width: auto;
      max-width: 7.25rem;
      max-height: 3.25rem;
      margin-right: 1rem;
    }
  }

  .menu-container {
    display: flex;
    flex-direction: column;
    justify-content: space-evenly;
    margin: 2rem 2rem 1rem;

    div {
      padding-bottom: 0.5rem;
      border-bottom: 0.25px solid lightgray;
    }

    div:not(:first-child) {
      margin-top: 8px;
    }

    a {
      padding-bottom: 0.25rem;
      font-size: 1.5rem;
      font-weight: ${props => props.theme.weight.regular};
    }
  }

  .button-container {
    display: flex;
    margin: 0.5rem 3rem;
    justify-content: space-between;
  }

  .button {
    width: 7rem;
    height: 2.75rem;
    display: flex;
    justify-content: center;
    border-radius: 10px;
    margin: 0 0.5rem;

    a {
      font-size: 1.25rem;
      margin: auto auto;
      color: white;
    }
  }

  .logout {
    background: #cf5061;
  }

  .back {
    background: #4461a7;
  }

  @media (max-width: 400px) {
    .menu-container {
      a {
        font-family: "Rubik";
        font-size: 1.25rem;
      }
    }
  }
`;
