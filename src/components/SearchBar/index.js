import React, { Component } from "react";
import { SearchBarStyle } from "./style";
import PropTypes from "prop-types";

import SearchIcon from "../../assets/icons/search.svg";
import Card from "../Card";

class SearchBar extends Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.props.onQueryChange(event.target.value);
  }

  render() {
    return (
      <SearchBarStyle>
        <Card>
          <img className="SearchBar__searchIcon" src={SearchIcon} />
          <input
            className="SearchBar__input"
            type="text"
            value={this.props.query}
            onChange={this.handleChange}
            placeholder={this.props.placeholder}
            onFocus={this.props.handleFocus}
            onBlur={this.props.handleBlur}
          />
        </Card>
      </SearchBarStyle>
    );
  }
}

SearchBar.propTypes = {
  placeholder: PropTypes.string.isRequired,
  onQueryChange: PropTypes.func.isRequired,
  query: PropTypes.string,
  handleFocus: PropTypes.func,
  handleBlur: PropTypes.func
};

export default SearchBar;
