import React, { Component } from "react";

import { StaffComponents } from "./style";
import PropTypes from "prop-types";

class Staff extends Component {
  render() {
    const { label, urlPhoto, jabatan } = this.props;

    return (
      <StaffComponents>
        <div className="staff-box">
          <div className="department-label"> {label} </div>
          <img className="staff-photo" src={urlPhoto} alt={jabatan} />
        </div>
      </StaffComponents>
    );
  }
}
Staff.propTypes = {
  label: PropTypes.string,
  urlPhoto: PropTypes.string,
  jabatan: PropTypes.string
};
export default Staff;
