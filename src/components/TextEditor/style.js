import styled from "styled-components";

export const TextEditorContainer = styled.div`
  padding: 1.5rem;
  box-shadow: ${props => props.theme.boxShadow};
  background: ${props => props.theme.colors.white};
  border-radius: 10px;
  margin: 1rem 0;
  position: relative;
  z-index: 3;

  .btn-container {
    display: flex;
    justify-content: flex-end;
    margin-top: 1.5rem;
  }

  @media (max-width: 450px) {
    padding: 1rem;

    .btn-container {
      margin-top: 1rem;
    }
  }
`;
