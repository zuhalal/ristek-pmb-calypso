/* eslint-disable consistent-return */
import React, { Component } from "react";
import { TextEditorContainer } from "./style";
import PropTypes from "prop-types";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import Button from "../Button";

class TextEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: ""
    };
  }

  handleChange = text => this.setState({ text });

  handleSubmit = () => {
    const submittedText = this.state.text;
    if (submittedText.length === 0) {
      return null;
    }
    this.props.onSubmitComment(submittedText);
    this.setState({ text: "" });
  };

  render() {
    return (
      <TextEditorContainer>
        <ReactQuill
          placeholder="Write your question or answer here..."
          theme="snow"
          value={this.state.text}
          onChange={this.handleChange}
        />
        <div className="btn-container">
          <Button onClick={this.handleSubmit} text="Submit" />
        </div>
      </TextEditorContainer>
    );
  }
}

TextEditor.propTypes = {
  onSubmitComment: PropTypes.func.isRequired
};

export default TextEditor;
