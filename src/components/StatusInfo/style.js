import styled from "styled-components";

export const StatusContainer = styled.div`
  background: ${props => props.color};
  border-radius: 20px;
  padding: 0.25rem 2rem;
  color: white;
  margin-top: 1.1em;

  h5 {
    margin: 0;
  }

  @media (max-width: 1216px) {
  }

  @media (max-width: 1024px) {
    padding: 0.3rem 2rem;
    margin-top: 0.7em;
  }

  @media (max-width: 580px) {
  }

  @media (max-width: 480px) {
  }
`;
