import React, { Component } from "react";
import PropTypes from "prop-types";
import { StatusContainer } from "./style";

class StatusInfo extends Component {
  colorPicker = status => {
    let color = "";
    switch (status.toUpperCase()) {
      case "PENDING":
        color = "#FFA400";
        break;
      case "ACCEPTED":
        color = "#32C238";
        break;
      case "REJECTED":
        color = "#F76B6B";
        break;
      case "SAVED":
        color = "#4461A7";
        break;
      default:
        color = "#999999";
    }

    return color;
  };

  getStatusText = () => {
    const text =
      this.props.status.charAt(0).toUpperCase() +
      this.props.status.slice(1).toLowerCase();

    return text.replace("_", " ");
  };

  render() {
    return (
      <StatusContainer color={this.colorPicker(this.props.status)}>
        <h5>{this.getStatusText()}</h5>
      </StatusContainer>
    );
  }
}

StatusInfo.propTypes = {
  status: PropTypes.string
};

export default StatusInfo;
