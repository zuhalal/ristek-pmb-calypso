/* eslint-disable array-callback-return */
import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { push } from "connected-react-router";
import EventCard from "components/EventCard";
import PaginationContainer from "components/PaginationContainer";

import { InternalEventSectionContainer } from "./style";
import { timestampParser } from "utils/timestampParser";

class InternalEventSection extends Component {
  renderUpcomingInternalEvents = () => {
    const now = new Date();
    const upcomingEvents = [];
    const pastEvents = [];

    if (this.props.events) {
      this.props.events.map(event => {
        const eventDate = new Date(event.date);
        const eventCard = (
          <div className="event-wrap" key={`{event-card-${event.id}}`}>
            <EventCard
              title={event.title}
              date={timestampParser(new Date(event.date))}
              location={event.place}
              onClick={() => this.props.push(`/events/${event.id}`)}
            />
          </div>
        );
        if (eventDate < now) {
          pastEvents.push(eventCard);
        } else {
          upcomingEvents.push(eventCard);
        }
      });
    }

    return {
      upcomingEvents,
      pastEvents
    };
  };

  render() {
    const { upcomingEvents, pastEvents } = this.renderUpcomingInternalEvents();

    return (
      <InternalEventSectionContainer>
        <PaginationContainer
          header={<p className="title">Upcoming Events</p>}
          isLoading={this.props.isLoading}
          emptyText={"There isn't any Upcoming Event"}
        >
          {upcomingEvents}
        </PaginationContainer>
        <PaginationContainer
          header={<p className="title">Past Events</p>}
          isLoading={this.props.isLoading}
          emptyText={"There isn't any Past Event"}
        >
          {pastEvents}
        </PaginationContainer>
      </InternalEventSectionContainer>
    );
  }
}

InternalEventSection.propTypes = {
  events: PropTypes.array.isRequired,
  push: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired
};

const mapDispatchToProps = dispatch => ({
  push: url => dispatch(push(url))
});

export default connect(
  null,
  mapDispatchToProps
)(InternalEventSection);
