/* eslint-disable no-mixed-operators */
/* eslint-disable no-plusplus */
/* eslint-disable no-unused-expressions */
/* eslint-disable id-length */
import React, { Component } from "react";
import PropTypes from "prop-types";
import { isEmpty } from "lodash";
import { PaginationContainerStyle } from "./style";
import Spinner from "components/Loading";
import EmptyCard from "components/EmptyCard";

class PaginationContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: 1,
      currentPageArray: 0,
      itemsPerPage: this.props.itemsPerPage || 5,
      paginationPerPage: 3
    };
  }

  countLastItemIndex = () => this.state.currentPage * this.state.itemsPerPage;

  countFirstItemIndex = () =>
    this.countLastItemIndex() - this.state.itemsPerPage;

  renderCurrentItems = () => {
    if (isEmpty(this.props.children)) {
      return <EmptyCard text={this.props.emptyText} />;
    }

    return this.props.children.slice(
      this.countFirstItemIndex(),
      this.countLastItemIndex()
    );
  };

  renderNumberComponent = number => (
    <a
      className={`clickable page-number-${
        this.state.currentPage === number ? "active" : "inactive"
      }`}
      onClick={() => this.setState({ currentPage: number })}
    >
      {number}
    </a>
  );

  movePageArray = addition => {
    const { currentPageArray, paginationPerPage } = this.state;
    const nextPageArrayIdx = currentPageArray + addition;
    const nextPage = 1 + nextPageArrayIdx * paginationPerPage;

    this.setState({
      currentPageArray: nextPageArrayIdx,
      currentPage: nextPage
    });
  };

  // eslint-disable-next-line no-confusing-arrow
  renderMovePageComponent = direction =>
    direction === "left" ? (
      <a className="clickable" onClick={() => this.movePageArray(-1)}>
        ...
      </a>
    ) : (
      <a className="clickable" onClick={() => this.movePageArray(1)}>
        ...
      </a>
    );

  renderArrayPageNumber = idx => {
    const totalItem = this.props.children.length;
    const { paginationPerPage, itemsPerPage } = this.state;
    const totalPages = Math.ceil(totalItem / itemsPerPage);

    const arr = [];
    let temp = [];

    for (let i = 1; i <= totalPages; i++) {
      temp.push(this.renderNumberComponent(i));

      if (i % paginationPerPage === 0 || i === totalPages) {
        if (i < totalPages) {
          temp.push(this.renderMovePageComponent("right"));
        }
        arr.push(temp);
        temp = [];
        temp.push(this.renderMovePageComponent("left"));
      }
    }

    return arr[idx];
  };

  render() {
    const arrPageNumber = this.renderArrayPageNumber(
      this.state.currentPageArray
    );

    return (
      <PaginationContainerStyle>
        <div className="pagination-header">
          <div>{this.props.header}</div>
          <div>
            {!isEmpty(arrPageNumber) && "<"}
            {this.renderArrayPageNumber(this.state.currentPageArray)}
            {!isEmpty(arrPageNumber) && ">"}
          </div>
        </div>
        {this.props.isNotification ? (
          <ul>
            {this.props.isLoading ? <Spinner /> : this.renderCurrentItems()}
          </ul>
        ) : (
          <div>
            {this.props.isLoading ? <Spinner /> : this.renderCurrentItems()}
          </div>
        )}
      </PaginationContainerStyle>
    );
  }
}

PaginationContainer.propTypes = {
  children: PropTypes.node.isRequired,
  header: PropTypes.element,
  isLoading: PropTypes.bool.isRequired,
  emptyText: PropTypes.string.isRequired,
  isNotification: PropTypes.bool,
  itemsPerPage: PropTypes.number
};

export default PaginationContainer;
