import styled from "styled-components";

export const PaginationContainerStyle = styled.div`
  width: 100%;
  margin-bottom: 1rem;

  .pagination-header {
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 1rem;
  }
  .clickable {
    margin: 0 0.25rem;
    cursor: pointer;
  }

  ul {
    padding: 0;
  }

  .page-number-active {
    font-weight: ${props => props.theme.weight.bold};
  }

  .page-number-inactive {
  }
`;
